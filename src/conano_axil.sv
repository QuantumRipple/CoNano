//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
//AXI-Lite register interface and fifo's to control and feed the underlying conano_engine
module conano_axil #(
   parameter TILES = 1
)(
   input  wire         clk_i  ,

   input  wire  [ 6:0] awaddr ,
   input  wire         awvalid,
   output logic        awready,

   input  wire  [31:0] wdata  ,
   input  wire         wvalid ,
   output logic        wready ,

   output logic [ 1:0] bresp  ,
   output logic        bvalid ,
   input  wire         bready ,

   input  wire  [ 6:0] araddr ,
   input  wire         arvalid,
   output logic        arready,

   output logic [31:0] rdata  ,
   output logic [ 1:0] rresp  ,
   output logic        rvalid ,
   input  wire         rready
);
   localparam logic [15:0] REVISION = {8'h00,4'h1,4'h0}; //major, minor, branch

   logic [383:0] ofifo_wdata;
   logic [383:0] ofifo_rdata;
   logic [383:0] ofifo_rdata_reg;
   logic         ofifo_rvalid;
   logic [256:0] ififo_wdata;
   logic [256:0] ififo_wdata_reg;
   logic [256:0] ififo_rdata;
   logic         ififo_rvalid;
   logic         ififo_wready;

   logic ififo_pop ;
   logic ififo_push=0;
   logic ofifo_pop =0;
   logic ofifo_push;

   logic [5:0] threshold_reg [2] = '{6'd41,6'd35};
   logic disable_reg=0;
   logic abort_reg  =0;
   logic clear_reg  =0;

   logic arready_i =1;
   logic rvalid_i  =0;
   logic awready_i =0;
   logic wready_i  =0;
   logic bvalid_i  =0;
   logic bvalid_pre=0;

   assign arready = arready_i;
   assign  rvalid =  rvalid_i;
   assign awready = awready_i;
   assign  wready =  wready_i;
   assign  bvalid =  bvalid_i;

   assign rresp = 2'b00;
   assign bresp = 2'b00;
   always @ (posedge clk_i) begin
      if (ififo_wready) ififo_push <= 0; //default, clear the push if not-full (i.e. write will be accepted)
      ofifo_pop  <= 0; //default
      awready_i  <= 0; //default
      wready_i   <= 0; //default
      bvalid_pre <= 0; //default
      abort_reg  <= 0; //default

      ofifo_rdata_reg <= ofifo_rdata; //for timing
      ififo_wdata     <= ififo_wdata_reg; //for timing

      if (rvalid_i && rready) begin
         rvalid_i  <= 0;
         arready_i <= 1;
      end
      if (arvalid && arready_i) begin
         rvalid_i  <= 1;
         arready_i <= 0;
      end
      case(araddr[6:2])
         5'h00 : rdata <= {"QR",REVISION}; //author & revision info
         5'h01 : rdata <= {24'b0, 3'b0,ififo_push,3'b0,ofifo_rvalid};//fifo control/status
         5'h02 : rdata <= {16'b0,2'b0,threshold_reg[1],2'b0,threshold_reg[0]};//thresholds
         5'h03 : rdata <= {28'b0,1'b0,disable_reg,clear_reg,1'b0};//system control/status

         5'h07 : rdata <= {31'b0,ififo_wdata_reg[256]};
         5'h08 : rdata <= ififo_wdata_reg[0*32+:32];
         5'h09 : rdata <= ififo_wdata_reg[1*32+:32];
         5'h0A : rdata <= ififo_wdata_reg[2*32+:32];
         5'h0B : rdata <= ififo_wdata_reg[3*32+:32];
         5'h0C : rdata <= ififo_wdata_reg[4*32+:32];
         5'h0D : rdata <= ififo_wdata_reg[5*32+:32];
         5'h0E : rdata <= ififo_wdata_reg[6*32+:32];
         5'h0F : rdata <= ififo_wdata_reg[7*32+:32];

         5'h10 : rdata <= ofifo_rdata_reg[ 0*32+:32];
         5'h11 : rdata <= ofifo_rdata_reg[ 1*32+:32];
         5'h12 : rdata <= ofifo_rdata_reg[ 2*32+:32];
         5'h13 : rdata <= ofifo_rdata_reg[ 3*32+:32];
         5'h14 : rdata <= ofifo_rdata_reg[ 4*32+:32];
         5'h15 : rdata <= ofifo_rdata_reg[ 5*32+:32];
         5'h16 : rdata <= ofifo_rdata_reg[ 6*32+:32];
         5'h17 : rdata <= ofifo_rdata_reg[ 7*32+:32];
         5'h18 : rdata <= ofifo_rdata_reg[ 8*32+:32];
         5'h19 : rdata <= ofifo_rdata_reg[ 9*32+:32];
         5'h1A : rdata <= ofifo_rdata_reg[10*32+:32];
         5'h1B : rdata <= ofifo_rdata_reg[11*32+:32];
         default: rdata <= 32'b0;
      endcase

      if (bvalid_pre) bvalid_i <= 1; //bvalid_pre exists because it's important bvalid rises strictly after the associated write transaction is seen to occur by both parties (so AFTER the next cycle)
      else if (bvalid_i && bready) bvalid_i <= 0;

      if (awvalid && wvalid && (!bvalid_i || bready) && !bvalid_pre) begin
         bvalid_pre <= 1;
         awready_i  <= 1;
         wready_i   <= 1;
         case (awaddr[6:2]) //even though the transaction will be SEEN next cycle, we can act on the data this cycle because the master may not lower their valids or change the data until it sees a transaction occur
            5'h01 : begin //fifo control/status
               if (wdata[1]) ofifo_pop  <= 1;
               if (wdata[4]) ififo_push <= 1;
            end
            5'h02 : {threshold_reg[0],threshold_reg[1]} <= {wdata[5:0],wdata[13:8]}; //thresholds
            5'h03 : {disable_reg,clear_reg,abort_reg}   <= wdata[2:0]; //system control/status

            5'h07 : ififo_wdata_reg[256]      <= wdata[0]   ;
            5'h08 : ififo_wdata_reg[0*32+:32] <= wdata[31:0];
            5'h09 : ififo_wdata_reg[1*32+:32] <= wdata[31:0];
            5'h0A : ififo_wdata_reg[2*32+:32] <= wdata[31:0];
            5'h0B : ififo_wdata_reg[3*32+:32] <= wdata[31:0];
            5'h0C : ififo_wdata_reg[4*32+:32] <= wdata[31:0];
            5'h0D : ififo_wdata_reg[5*32+:32] <= wdata[31:0];
            5'h0E : ififo_wdata_reg[6*32+:32] <= wdata[31:0];
            5'h0F : ififo_wdata_reg[7*32+:32] <= wdata[31:0];
            default : ;
         endcase
      end
   end

   inferred_fifo #(
      .DEPTH_LOG2( 9),
      .WIDTH     (257)
   ) u_ififo (
      .clk_i   (clk_i       ),
      .wdata_i (ififo_wdata ),
      .pop_i   (ififo_pop   ),
      .push_i  (ififo_push  ),
      .rdata_o (ififo_rdata ),
      .rvalid_o(ififo_rvalid),
      .wready_o(ififo_wready)
   );

   inferred_fifo #(
      .DEPTH_LOG2( 9),
      .WIDTH     (384)
   ) u_ofifo (
      .clk_i   (clk_i       ),
      .wdata_i (ofifo_wdata ),
      .pop_i   (ofifo_pop   ),
      .push_i  (ofifo_push  ),
      .rdata_o (ofifo_rdata ),
      .rvalid_o(ofifo_rvalid),
      .wready_o(            )
   );

   conano_engine #(
      .TILES(TILES) //scales up copies of the calculation engine, which linearly increases both performance and area consumed. Interface remains unchanged.
   ) u_engine (
      .clk_i          (clk_i),
      .ififo_valid_i  (ififo_rvalid ),
      .ififo_data_i   (ififo_rdata  ),
      .ififo_pop_o    (ififo_pop    ),
      .ofifo_push_o   (ofifo_push   ),
      .ofifo_data_o   (ofifo_wdata  ),
      .threshold_reg_i(threshold_reg), //indicates how many places the hash is shifted down before being compared to 0. i.e. a value of 15 would match a difficulty of 0xFFFF_FFFF_FFFF_8000
      .abort_reg_i    (abort_reg    ), //should only be valid for a 1-cycle pulse. causes the previous_hash currently being evaluated to be abandoned early.
      .clear_reg_i    (clear_reg    ),
      .disable_reg_i  (disable_reg  )
   );
endmodule
`default_nettype wire
