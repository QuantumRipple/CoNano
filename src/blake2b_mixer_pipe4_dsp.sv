//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module blake2b_mixer_pipe4_dsp ( //no CREG so the XOR operations can happen before the DSP (after registering A)
   input  uwire logic        clk_i,
   input  uwire logic [63:0] ai,bi,ci,di,xi,yil2,
   output       logic [63:0] ao,bo,co,dou
);
   logic [63:0] a [1:4];
   logic [63:0] b [1:4];
   logic [63:0] c [1:4];
   logic [63:0] d [1:4];
   logic [63:0] bn [3:5];
   logic [63:0] dn [2:4];
   logic [63:0] cn [2:4];
   logic c2carryn,c4carryn;

   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (1),
      .NUM_CREG  (0),
      .NUM_CIREG (0),
      .NUM_PREG  (1)
   ) u_dsp0 (
      .clk (clk_i       ),
      .abci(48'b0       ),
      .ab  (ci[63:16]   ),
      .abco(            ),
      .c   (dn[2][63:16]),
      .ci  (c2carryn    ),
      .co  (            ),
      .pci (48'b0       ),
      .p   (c[2][63:16] ),
      .pco (            ),
      .opmode(7'b0001111)
   );
   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (1),
      .NUM_CREG  (0),
      .NUM_CIREG (0),
      .NUM_PREG  (1)
   ) u_dsp1 (
      .clk (clk_i       ),
      .abci(48'b0       ),
      .ab  (c[2][63:16] ),
      .abco(            ),
      .c   (dn[4][63:16]),
      .ci  (c4carryn    ),
      .co  (            ),
      .pci (48'b0       ),
      .p   (c[4][63:16] ),
      .pco (            ),
      .opmode(7'b0001111)
   );
   always_comb begin
      dn[2] = {2{d[1]^a[1]}} >> 32 ;
      {c2carryn,cn[2][15:0]} = c[1][15:0] + dn[2][15:0];
      bn[3] = {2{b[2]^c[2]}}>>24;
      dn[4] = {2{d[3]^a[3]}}>>16 ;
      {c4carryn,cn[4][15:0]} = c[3][15:0] + dn[4][15:0];
      bn[5] = {2{b[4]^c[4]}} >> 63;  //combinatorial output to shift this into the fabric add of the next mixer
   end
   always @ (posedge clk_i) begin
      a[1] <= ai + bi + xi;
      b[1] <= bi;
      c[1] <= ci;
      d[1] <= di;
      a[2] <= a[1];
      b[2] <= b[1];
      c[2][15:0] <= cn[2][15:0];
      d[2] <= dn[2];
      a[3] <= a[2]+bn[3]+yil2;
      b[3] <= bn[3];
      c[3] <= c[2];
      d[3] <= d[2];
      a[4] <= a[3];
      b[4] <= b[3];
      c[4][15:0] <= cn[4][15:0];
      d[4] <= dn[4];
   end
   assign ao  = a[4];
   assign bo  = bn[5];
   assign co  = c[4];
   assign dou = d[4];
endmodule
`default_nettype wire
