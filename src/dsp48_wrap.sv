//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module dsp48_wrap #( //wraps the Xilinx DSP48E1 primitive with a smaller (although greatly limited) interface.
   parameter AB_CASCADE = 0,
   parameter NUM_ABREG  = 1,
   parameter NUM_CREG   = 1,
   parameter NUM_CIREG  = 0,
   parameter NUM_PREG   = 1
)(
   input uwire logic       clk ,
   input uwire logic[47:0] abci,
   input uwire logic[47:0] ab  ,
   output      logic[47:0] abco,
   input uwire logic[47:0] c   ,
   input uwire logic       ci  ,
   output      logic       co  ,
   input uwire logic[47:0] pci ,
   output      logic[47:0] p   ,
   output      logic[47:0] pco ,
   input uwire logic[ 6:0] opmode
);

logic [3:0] co_simd;
assign co = co_simd[0];

DSP48E1#(
   //FeatureControlAttributes:DataPathSelection
   .A_INPUT  (AB_CASCADE?"CASCADE":"DIRECT"),//SelectsAinputsource,"DIRECT"(Aport)or"CASCADE"(ACINport)
   .B_INPUT  (AB_CASCADE?"CASCADE":"DIRECT"),//SelectsBinputsource,"DIRECT"(Bport)or"CASCADE"(BCINport)
   .USE_DPORT("FALSE"                      ),//SelectDportusage(TRUEorFALSE)
   .USE_MULT ("NONE"                       ),//Selectmultiplierusage("MULTIPLY","DYNAMIC",or"NONE")
   //PatternDetectorAttributes:PatternDetectionConfiguration
   .AUTORESET_PATDET  ("NO_RESET"      ),//"NO_RESET","RESET_MATCH","RESET_NOT_MATCH"
   .MASK              (48'hffffffffffff),//48-bitmaskvalueforpatterndetect(1=ignore)
   .PATTERN           (48'h000000000000),//48-bitpatternmatchforpatterndetect
   .SEL_MASK          ("MASK"          ),//"C","MASK","ROUNDING_MODE1","ROUNDING_MODE2"
   .SEL_PATTERN       ("PATTERN"       ),//Selectpatternvalue("PATTERN"or"C")
   .USE_PATTERN_DETECT("NO_PATDET"     ),//Enablepatterndetect("PATDET"or"NO_PATDET")
   //RegisterControlAttributes:PipelineRegisterConfiguration
   .ACASCREG     (NUM_ABREG),//NumberofpipelinestagesbetweenA/ACINandACOUT(0,1or2)
   .ADREG        (1        ),//Numberofpipelinestagesforpre-adder(0or1)
   .ALUMODEREG   (1        ),//NumberofpipelinestagesforALUMODE(0or1)
   .AREG         (NUM_ABREG),//NumberofpipelinestagesforA(0,1or2)
   .BCASCREG     (NUM_ABREG),//NumberofpipelinestagesbetweenB/BCINandBCOUT(0,1or2)
   .BREG         (NUM_ABREG),//NumberofpipelinestagesforB(0,1or2)
   .CARRYINREG   (NUM_CIREG),//NumberofpipelinestagesforCARRYIN(0or1)
   .CARRYINSELREG(1        ),//NumberofpipelinestagesforCARRYINSEL(0or1)
   .CREG         (NUM_CREG ),//NumberofpipelinestagesforC(0or1)
   .DREG         (1        ),//NumberofpipelinestagesforD(0or1)
   .INMODEREG    (1        ),//NumberofpipelinestagesforINMODE(0or1)
   .MREG         (0        ),//Numberofmultiplierpipelinestages(0or1)
   .OPMODEREG    (1        ),//NumberofpipelinestagesforOPMODE(0or1)
   .PREG         (NUM_PREG ),//NumberofpipelinestagesforP(0or1)
   .USE_SIMD     ("ONE48"  ) //SIMDselection("ONE48","TWO24","FOUR12")
)DSP48E1_inst(
   //Cascade:30-bit(each)output:CascadePorts
   .ACOUT       (abco[47:18]),//30-bitoutput:Aportcascadeoutput
   .BCOUT       (abco[17: 0]),//18-bitoutput:Bportcascadeoutput
   .CARRYCASCOUT(           ),//1-bitoutput:Cascadecarryoutput
   .MULTSIGNOUT (           ),//1-bitoutput:Multipliersigncascadeoutput
   .PCOUT       (pco        ),//48-bitoutput:Cascadeoutput
   //Control:1-bit(each)output:ControlInputs/StatusBits
   .OVERFLOW      ( ),//1-bitoutput:Overflowinadd/accoutput
   .PATTERNBDETECT( ),//1-bitoutput:Patternbardetectoutput
   .PATTERNDETECT ( ),//1-bitoutput:Patterndetectoutput
   .UNDERFLOW     ( ),//1-bitoutput:Underflowinadd/accoutput
   //Data:4-bit(each)output:DataPorts
   .CARRYOUT(co_simd),//4-bitoutput:Carryoutput
   .P       (p      ),//48-bitoutput:Primarydataoutput
   //Cascade:30-bit(each)input:CascadePorts
   .ACIN       (abci[47:18]),//30-bitinput:Acascadedatainput
   .BCIN       (abci[17: 0]),//18-bitinput:Bcascadeinput
   .CARRYCASCIN(1'b0       ),//1-bitinput:Cascadecarryinput
   .MULTSIGNIN (1'b0       ),//1-bitinput:Multipliersigninput
   .PCIN       (pci        ),//48-bitinput:Pcascadeinput
   //Control:4-bit(each)input:ControlInputs/StatusBits
   .ALUMODE   (4'b0000 ),//4-bitinput:ALUcontrolinput
   .CARRYINSEL(3'b000  ),//3-bitinput:Carryselectinput
   .CEINMODE  (1'b1    ),//1-bitinput:ClockenableinputforINMODEREG
   .CLK       (clk     ),//1-bitinput:Clockinput
   .INMODE    (5'b00010),//5-bitinput:INMODEcontrolinput
   .OPMODE    (opmode  ),//7-bitinput:Operationmodeinput
   .RSTINMODE (1'b0    ),//1-bitinput:ResetinputforINMODEREG
   //Data:30-bit(each)input:DataPorts
   .A      (ab[47:18]),//30-bitinput:Adatainput
   .B      (ab[17: 0]),//18-bitinput:Bdatainput
   .C      (c        ),//48-bitinput:Cdatainput
   .CARRYIN(ci       ),//1-bitinput:Carryinputsignal
   .D      (25'b0    ),//25-bitinput:Ddatainput
   //Reset/ClockEnable:1-bit(each)input:Reset/ClockEnableInputs
   .CEA1         (NUM_ABREG>1),//1-bitinput:Clockenableinputfor1ststageAREG
   .CEA2         (NUM_ABREG>0),//1-bitinput:Clockenableinputfor2ndstageAREG
   .CEAD         (1'b0),//1-bitinput:ClockenableinputforADREG
   .CEALUMODE    (1'b1),//1-bitinput:ClockenableinputforALUMODERE
   .CEB1         (NUM_ABREG>1),//1-bitinput:Clockenableinputfor1ststageBREG
   .CEB2         (NUM_ABREG>0),//1-bitinput:Clockenableinputfor2ndstageBREG
   .CEC          (NUM_CREG),//1-bitinput:ClockenableinputforCREG
   .CECARRYIN    (NUM_CIREG),//1-bitinput:ClockenableinputforCARRYINREG
   .CECTRL       (1'b1),//1-bitinput:ClockenableinputforOPMODEREGandCARRYINSELREG
   .CED          (1'b0),//1-bitinput:ClockenableinputforDREG
   .CEM          (1'b0),//1-bitinput:ClockenableinputforMREG
   .CEP          (NUM_PREG),//1-bitinput:ClockenableinputforPREG
   .RSTA         (1'b0),//1-bitinput:ResetinputforAREG
   .RSTALLCARRYIN(1'b0),//1-bitinput:ResetinputforCARRYINREG
   .RSTALUMODE   (1'b0),//1-bitinput:ResetinputforALUMODEREG
   .RSTB         (1'b0),//1-bitinput:ResetinputforBREG
   .RSTC         (1'b0),//1-bitinput:ResetinputforCREG
   .RSTCTRL      (1'b0),//1-bitinput:ResetinputforOPMODEREGandCARRYINSELREG
   .RSTD         (1'b0),//1-bitinput:ResetinputforDREGandADREG
   .RSTM         (1'b0),//1-bitinput:ResetinputforMREG
   .RSTP         (1'b0) //1-bitinput:ResetinputforPREG
);//EndofDSP48E1_instinstantiation

endmodule
`default_nettype wire
