//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module conano_pynqz2_top (
   input wire       sysclk     ,
   inout wire [14:0]DDR_addr   ,
   inout wire [2:0] DDR_ba     ,
   inout wire       DDR_cas_n  ,
   inout wire       DDR_ck_n   ,
   inout wire       DDR_ck_p   ,
   inout wire       DDR_cke    ,
   inout wire       DDR_cs_n   ,
   inout wire [3:0] DDR_dm     ,
   inout wire [31:0]DDR_dq     ,
   inout wire [3:0] DDR_dqs_n  ,
   inout wire [3:0] DDR_dqs_p  ,
   inout wire       DDR_odt    ,
   inout wire       DDR_ras_n  ,
   inout wire       DDR_reset_n,
   inout wire       DDR_we_n   ,
   inout wire       FIXED_IO_ddr_vrn,
   inout wire       FIXED_IO_ddr_vrp,
   inout wire [53:0]FIXED_IO_mio    ,
   inout wire       FIXED_IO_ps_clk ,
   inout wire       FIXED_IO_ps_porb,
   inout wire       FIXED_IO_ps_srstb
);
   /*
   logic clk;

   logic  [31:0] araddr;
   logic         arready;
   logic         arvalid;

   logic  [31:0] awaddr;
   logic         awready;
   logic         awvalid;

   logic         bready;
   logic  [1:0]  bresp;
   logic         bvalid;

   logic  [31:0] rdata;
   logic         rready;
   logic  [1:0]  rresp;
   logic         rvalid;

   logic  [31:0] wdata;
   logic         wready;
   logic         wvalid;

   logic  [0:0] axi_rst;
   */

   conano_pynqz2_bd u_bd (
      .DDR_addr   (DDR_addr   ),
      .DDR_ba     (DDR_ba     ),
      .DDR_cas_n  (DDR_cas_n  ),
      .DDR_ck_n   (DDR_ck_n   ),
      .DDR_ck_p   (DDR_ck_p   ),
      .DDR_cke    (DDR_cke    ),
      .DDR_cs_n   (DDR_cs_n   ),
      .DDR_dm     (DDR_dm     ),
      .DDR_dq     (DDR_dq     ),
      .DDR_dqs_n  (DDR_dqs_n  ),
      .DDR_dqs_p  (DDR_dqs_p  ),
      .DDR_odt    (DDR_odt    ),
      .DDR_ras_n  (DDR_ras_n  ),
      .DDR_reset_n(DDR_reset_n),
      .DDR_we_n   (DDR_we_n   ),
      .FIXED_IO_ddr_vrn (FIXED_IO_ddr_vrn ),
      .FIXED_IO_ddr_vrp (FIXED_IO_ddr_vrp ),
      .FIXED_IO_mio     (FIXED_IO_mio     ),
      .FIXED_IO_ps_clk  (FIXED_IO_ps_clk  ),
      .FIXED_IO_ps_porb (FIXED_IO_ps_porb ),
      .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb)
      /*
      ,.M_AXI_0_araddr (araddr ),
      .M_AXI_0_arprot (       ), //unused
      .M_AXI_0_arready(arready),
      .M_AXI_0_arvalid(arvalid),
      .M_AXI_0_awaddr (awaddr ),
      .M_AXI_0_awprot (       ), //unused
      .M_AXI_0_awready(awready),
      .M_AXI_0_awvalid(awvalid),
      .M_AXI_0_bready (bready ),
      .M_AXI_0_bresp  (bresp  ),
      .M_AXI_0_bvalid (bvalid ),
      .M_AXI_0_rdata  (rdata  ),
      .M_AXI_0_rready (rready ),
      .M_AXI_0_rresp  (rresp  ),
      .M_AXI_0_rvalid (rvalid ),
      .M_AXI_0_wdata  (wdata  ),
      .M_AXI_0_wready (wready ),
      .M_AXI_0_wstrb  (       ), //unused
      .M_AXI_0_wvalid (wvalid ),
      .axi_rst        (       ),//TODO
      .clk_o          (clk    )
      */
      ,.clk_i          (sysclk )
    );

   /*
   conano_axil u_conano(
      .clk_i  (clk),

      .awaddr (awaddr[6:0]),
      .awvalid,
      .awready,

      .wdata  ,
      .wvalid ,
      .wready ,

      .bresp  ,
      .bvalid ,
      .bready ,

      .araddr (araddr[6:0]),
      .arvalid,
      .arready,

      .rdata  ,
      .rresp  ,
      .rvalid ,
      .rready
   );
   */
endmodule
`default_nettype wire
