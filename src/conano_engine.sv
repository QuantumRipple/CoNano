//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
//performs difficulty check on the blake2b output, handles counter and pipeline, expands and contracts datapath to scale to arbitrary number of blake2b blocks
module conano_engine #(
   parameter TILES = 1 //scales up copies of the calculation engine, which directly increases both performance and area consumed (linearly with 2**TILES_LOG2). Interface remains unchanged.
)(
   input  uwire logic         clk_i          ,
   input  uwire logic         ififo_valid_i  ,
   input  uwire logic [256:0] ififo_data_i   ,
   output       logic         ififo_pop_o    ,
   output       logic         ofifo_push_o   ,
   output       logic [383:0] ofifo_data_o   ,
   input  uwire logic [  5:0] threshold_reg_i [2], //indicates how many places the hash is shifted down before being compared to 0. i.e. a value of 15 would match a difficulty of 0xFFFF_FFFF_FFFF_8000
   input  uwire logic         abort_reg_i    , //should only be valid for a 1-cycle pulse. causes the previous_hash currently being evaluated to be abandoned early.
   input  uwire logic         clear_reg_i    ,
   input  uwire logic         disable_reg_i
);
   localparam TILES_LOG2 = $clog2(TILES);
   localparam COUNTER_WIDTH = 64-TILES_LOG2;
   localparam LATENCY = 97;

   logic [COUNTER_WIDTH-1:0] counter = 0;
   logic [COUNTER_WIDTH-1:0] counter_mlat;
   logic [63:0] hash        [TILES];
   logic [63:0] hash_d1     [TILES];
   logic [63:0] hash_d2     [TILES];
   logic hash_below_thresh  [TILES];

   logic [5:0] thresh_shift;

   logic [255:0] previous_hash;
   logic         valid_d   [LATENCY+3];
   logic [0:0]   thresh_sel;

   logic         ififo_pop ;
   logic         ofifo_push;
   logic [383:0] ofifo_data;

   always @ (posedge clk_i) begin
      ififo_pop    <= 0; //default
      ofifo_push   <= 0; //default
      counter      <= counter+1;
      counter_mlat <= counter-LATENCY-1; //reduces logic in the decision path by precomputing this

      valid_d        [1:LATENCY+2] <= valid_d        [0:LATENCY+1];
      hash_d1 <= hash;
      hash_d2 <= hash_d1;

      if (abort_reg_i || clear_reg_i) begin //removed roll detection/skipping because it would take years on a single previous_hash to roll the nonce even at 10 billion hashes/s.
         ififo_pop <= 1;
      end

      previous_hash<= ififo_data_i[255:0];
      thresh_sel   <= ififo_data_i[256];
      valid_d[0]   <= ififo_valid_i && !disable_reg_i;

      if (ififo_pop) begin
         valid_d <= '{(LATENCY+3){1'b0}}; //changing chunk[0:3] (performing a pop) corrupts the pipeline, so must explicitly invalidate the whole thing
      end

      ofifo_data[319:0] <= {counter_mlat, previous_hash}; //only read when given a push
      for (int i=0; i<TILES; i++) begin
         if (valid_d[LATENCY+2] && hash_below_thresh[i]) begin
            if (TILES_LOG2 > 0) ofifo_data[319-:TILES_LOG2] <= i; //override tile bits with the actual tile that found the PoW. If multiple tiles found a PoW simultaneously, the highest numbered tile will win.
            ofifo_data[383:320] <= hash_d2[i];
            ififo_pop  <= 1;
            valid_d[LATENCY+2]<=1'b0; //just to clear out a potential second hit for this previous_hash before ififo_pop takes effect - we only need 1
            ofifo_push <= !disable_reg_i;
         end
      end
      thresh_shift <= threshold_reg_i[thresh_sel]; //registering this 2:1 mux slightly reduces path length of the dynamic hash shifting operation.
   end

   genvar j;
   for (j=0; j<TILES; j++) begin
      localparam logic [TILES_LOG2:0] TILE_INDEX = j; //1 bit wider than needed to avoid null-width. top bit will be truncated on port assignment.
      logic [63:0] shifted_hash;
      conano_blake2b #(
         .EXPECTED_LATENCY(LATENCY)
      ) u_tile (
         .clk_i       (clk_i               ),
         .hash_i      (previous_hash       ),
         .nonce_i     ({TILE_INDEX,counter}),
         .hash_o      (hash[j]             )
      );
      always @ (posedge clk_i) begin
         shifted_hash         <= (~hash[j]) >> thresh_shift;
         hash_below_thresh[j] <= !shifted_hash;
      end
   end
   assign ififo_pop_o  = ififo_pop;
   assign ofifo_push_o = ofifo_push;
   assign ofifo_data_o = ofifo_data;
endmodule
`default_nettype wire
