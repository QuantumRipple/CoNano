//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module conano_blake2b #(
   parameter EXPECTED_LATENCY=-1 //if >=0, asserts if expected latency does not equal actual latency
)(
   input  uwire logic        clk_i  ,
   input  uwire logic[255:0] hash_i ,//changing this value corrupts entire pipeline. no delay pipe saves resources as it (very) rarely changes
   input  uwire logic[ 63:0] nonce_i,
   output logic[63:0]        hash_o  //undergoes a pipeline delay relative to nonce_i
);
   typedef logic [63:0] t_blake_vec [8];
   typedef logic [63:0] t_bcomp_vec [16];
   typedef int t_sigma_vec [10][16];

   localparam t_blake_vec IV = '{
      64'h6a09e667f3bcc908,
      64'hbb67ae8584caa73b,
      64'h3c6ef372fe94f82b,
      64'ha54ff53a5f1d36f1,
      64'h510e527fade682d1,
      64'h9b05688c2b3e6c1f,
      64'h1f83d9abfb41bd6b,
      64'h5be0cd19137e2179
   };

   function t_blake_vec f_modify_iv;
      t_blake_vec temp;
      temp    = IV;
      temp[0] = temp[0] ^ 64'h0101_00_08; //0 bytes of key, 8 byte hash output
      return temp;
   endfunction
   localparam t_blake_vec MOD_IV = f_modify_iv();

   function t_bcomp_vec f_init_nano_state;
      t_bcomp_vec v;
      v[0: 7] = MOD_IV;
      v[8:15] = IV;
      v[12]   = v[12] ^ 64'd40; //fixed at 40 bytes for Nano //skip V13, as fixed 40 has no upper bits.
      v[14]   = ~v[14]; //we are always the last block, since we're hashing <= 128 bytes
      return v;
   endfunction
   localparam t_bcomp_vec NANO_INIT = f_init_nano_state();

   localparam t_sigma_vec S = '{ //sigma
      '{ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15},
      '{14, 10,  4,  8,  9, 15, 13,  6,  1, 12,  0,  2, 11,  7,  5,  3},
      '{11,  8, 12,  0,  5,  2, 15, 13, 10, 14,  3,  6,  7,  1,  9,  4},
      '{ 7,  9,  3,  1, 13, 12, 11, 14,  2,  6,  5, 10,  4,  0, 15,  8},
      '{ 9,  0,  5,  7,  2,  4, 10, 15, 14,  1, 11, 12,  6,  8,  3, 13},
      '{ 2, 12,  6, 10,  0, 11,  8,  3,  4, 13,  7,  5, 15, 14,  1,  9},
      '{12,  5,  1, 15, 14, 13,  4, 10,  0,  7,  6,  3,  9,  2,  8, 11},
      '{13, 11,  7, 14, 12,  1,  3,  9,  5,  0, 15,  4,  8,  6,  2, 10},
      '{ 6, 15, 14,  9, 11,  3,  0,  8, 12,  2, 13,  7,  1,  4, 10,  5},
      '{10,  2,  8,  4,  7,  6,  1,  5, 15, 11,  9, 14,  3, 12, 13,  0}
   };

   localparam MIX_DELAY       = 4; //delay of mixer module
   localparam MIX_STAGGER     = 0;//if c come in a cycle later and needs to be aligned at end of pipe
   localparam MIX_YLATE       = 2;//cycles to shift the Y input to the mixer to line up with the mixer's expectation. Moving the delay out of the mixer itself allows us to calculate instead of register as an optimization option
   localparam ADD_REG         = 0;//setting to 1 adds a register layer between rounds. setting to 2 adds a register layer between mixer blocks (two layers per round)
   localparam STEPS_PER_ROUND = 2+ADD_REG;
   localparam CALC_LATENCY    = 24*MIX_DELAY+12*ADD_REG+MIX_STAGGER+1;
   if (EXPECTED_LATENCY >= 0) initial assert (EXPECTED_LATENCY==CALC_LATENCY) else $error("latency mismatch");

   logic[63:0] count_d [CALC_LATENCY];
   t_bcomp_vec v       [12*STEPS_PER_ROUND+1]; //this delay pipe operates in "steps" rather than cycles. Each step could have 0 cycle delay or many from the last one.
   logic[63:0] destagger;
   assign count_d[0] = nonce_i;
   assign v[0] = NANO_INIT; //being constant, we don't have to shift to adjust for stagger at entry
   always @ (posedge clk_i) count_d[1:CALC_LATENCY-1] <= count_d[0:CALC_LATENCY-2];

   genvar ROUND;
   for (ROUND=0; ROUND<12; ROUND++) begin : g_rounds
      localparam D = ROUND*STEPS_PER_ROUND; //current local delay steps for the first mixer group's input (not a specific number of cycles)
      localparam D2 = D+1+(ADD_REG/2); //local delay steps for the 2nd set of mixers
      t_bcomp_vec mx,my,mdx,mdy;

      assign mx[0]    = count_d[ROUND*(2*MIX_DELAY+ADD_REG)];
      assign mx[1: 4] = {<<64{hash_i}};
      assign mx[5:15] = '{11{64'b0}};
      assign my[0]    = count_d[ROUND*(2*MIX_DELAY+ADD_REG)+MIX_YLATE];
      assign my[1: 4] = {<<64{hash_i}};
      assign my[5:15] = '{11{64'b0}};
      // Rounds 10 and 11 use SIGMA[0] and SIGMA[1] respectively
      blake2b_mixer_pipe4_dsp u_mix00(clk_i, v[D][0], v[D][4], v[D][ 8], v[D][12], mx[S[ROUND%10][ 0]], my[S[ROUND%10][ 1]], v[D+1][0], v[D+1][4], v[D+1][ 8], v[D+1][12]);
      blake2b_mixer_pipe4_dsp u_mix01(clk_i, v[D][1], v[D][5], v[D][ 9], v[D][13], mx[S[ROUND%10][ 2]], my[S[ROUND%10][ 3]], v[D+1][1], v[D+1][5], v[D+1][ 9], v[D+1][13]);
      blake2b_mixer_pipe4_dsp u_mix02(clk_i, v[D][2], v[D][6], v[D][10], v[D][14], mx[S[ROUND%10][ 4]], my[S[ROUND%10][ 5]], v[D+1][2], v[D+1][6], v[D+1][10], v[D+1][14]);
      blake2b_mixer_pipe4_dsp u_mix03(clk_i, v[D][3], v[D][7], v[D][11], v[D][15], mx[S[ROUND%10][ 6]], my[S[ROUND%10][ 7]], v[D+1][3], v[D+1][7], v[D+1][11], v[D+1][15]);
      if (ADD_REG>1) always @ (posedge clk_i) v[D+2] <= v[D+1];

      assign mdx[0]    = count_d[ROUND*(2*MIX_DELAY+ADD_REG)+MIX_DELAY+(ADD_REG/2)];
      assign mdx[1: 4] = {<<64{hash_i}};
      assign mdx[5:15] = '{11{64'b0}};
      assign mdy[0]    = count_d[ROUND*(2*MIX_DELAY+ADD_REG)+MIX_DELAY+(ADD_REG/2)+MIX_YLATE];
      assign mdy[1: 4] = {<<64{hash_i}};
      assign mdy[5:15] = '{11{64'b0}};
      //2nd group shares inputs with first group, so sequentially dependent
      blake2b_mixer_pipe4_dsp u_mix10(clk_i, v[D2][0], v[D2][5], v[D2][10], v[D2][15], mdx[S[ROUND%10][ 8]], mdy[S[ROUND%10][ 9]], v[D2+1][0], v[D2+1][5], v[D2+1][10], v[D2+1][15]);
      blake2b_mixer_pipe4_dsp u_mix11(clk_i, v[D2][1], v[D2][6], v[D2][11], v[D2][12], mdx[S[ROUND%10][10]], mdy[S[ROUND%10][11]], v[D2+1][1], v[D2+1][6], v[D2+1][11], v[D2+1][12]);
      blake2b_mixer_pipe4_dsp u_mix12(clk_i, v[D2][2], v[D2][7], v[D2][ 8], v[D2][13], mdx[S[ROUND%10][12]], mdy[S[ROUND%10][13]], v[D2+1][2], v[D2+1][7], v[D2+1][ 8], v[D2+1][13]);
      blake2b_mixer_pipe4_dsp u_mix13(clk_i, v[D2][3], v[D2][4], v[D2][ 9], v[D2][14], mdx[S[ROUND%10][14]], mdy[S[ROUND%10][15]], v[D2+1][3], v[D2+1][4], v[D2+1][ 9], v[D2+1][14]);
      if (ADD_REG>0) always @ (posedge clk_i) v[D2+2] <= v[D2+1];
   end

   always @ (posedge clk_i) destagger <= v[12*STEPS_PER_ROUND][0];//if stagger, v[8] (c) comes out a cycle late, so need to register v[0] (a)
   always @ (posedge clk_i) hash_o <= MOD_IV[0] ^ (MIX_STAGGER?destagger:v[12*STEPS_PER_ROUND][0]) ^ v[12*STEPS_PER_ROUND][8]; //we only need to compute the final xor for word 0 since we only need an 8-byte hash.
endmodule
`default_nettype wire
