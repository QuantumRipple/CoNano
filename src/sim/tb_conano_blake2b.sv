//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module tb_conano_blake2b ();
   logic         clk=0;
   always #5 clk = ~clk;
   localparam LATENCY = 97;

   logic [255:0] previous_hash;
   logic [ 63:0] nonce;
   logic [ 63:0] nonce_steady;
   logic [ 63:0] hash;
   logic [ 63:0] hash_steady;
   int delay_count=0;

   conano_blake2b #(
      .EXPECTED_LATENCY(LATENCY) //if >=0, asserts if expected latency does not equal actual latency
   ) u_uut (
      .clk_i  (clk          ),
      .hash_i (previous_hash),
      .nonce_i(nonce        ),
      .hash_o (hash         ) //on a ? cycle delay
   );

   initial begin
      #100
      @(posedge clk) #0;
      nonce = 64'h2bf29ef00786a6bc;
      nonce_steady = 64'h2bf29ef00786a6bc;
      previous_hash = {<<8{256'h718CC2121C3E6410_59BC1C2CFC45666C_99E8AE922F7A807B_7D07B62C995D79E2}};
      @(posedge clk) #0;
      delay_count +=1;
      nonce       = 64'b0;
      repeat(96) @(posedge clk) #0 delay_count +=1;

      assert(hash==64'hffffffd21c3933f4) else $error("hash output mismatch");

      repeat(10) @(posedge clk) #0;
      $stop;
   end

   //allows differential simulation between variant that only has a single cycle of valid nonce and steady state to debug pipeline delay mismatches
   conano_blake2b #(
      .EXPECTED_LATENCY(LATENCY) //if >=0, asserts if expected latency does not equal actual latency
   ) u_uut_comp (
      .clk_i  (clk          ),
      .hash_i (previous_hash),
      .nonce_i(nonce_steady ),
      .hash_o (hash_steady  ) //on a ? cycle delay
   );
endmodule
`default_nettype wire
