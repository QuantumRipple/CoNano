//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module sim_axil_master ( //limited function / not general purpose. Does not support configurable address/data width, byte enables, response reporting
   input uwire logic        clk_i    ,

   output      logic [31:0] awaddr   ,
   output      logic        awvalid=0,
   input uwire logic        awready  ,

   output      logic [31:0] wdata    ,
   output      logic        wvalid =0,
   input uwire logic        wready   ,

   input uwire logic [ 1:0] bresp    ,
   input uwire logic        bvalid   ,
   output      logic        bready =0,

   output      logic [31:0] araddr   ,
   output      logic        arvalid=0,
   input uwire logic        arready  ,

   input uwire logic [31:0] rdata    ,
   input uwire logic [ 1:0] rresp    ,
   input uwire logic        rvalid   ,
   output      logic        rready =0
);
   task read(input [31:0] addr, output [31:0] data);
      @(posedge clk_i) #0;
      araddr  = addr;
      arvalid = 1;
      @(posedge clk_i iff arready) #0 arvalid = 0;
      rready = 1;
      @(posedge clk_i iff rvalid) #0 rready = 0;
      data = rdata;
   endtask
   task write(input [31:0] addr, input [31:0] data);
      @(posedge clk_i) #0;
      awaddr  = addr;
      wdata   = data;
      awvalid = 1;
      wvalid  = 1;
      fork
         @ (posedge clk_i iff awready) #0 awvalid = 0;
         @ (posedge clk_i iff  wready) #0  wvalid = 0;
      join
      bready = 1;
      @(posedge clk_i iff bvalid) #0 bready = 0;
   endtask
endmodule
`default_nettype wire
