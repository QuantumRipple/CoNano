//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module tb_conano_axil ();
   logic         clk=0;
   always #5 clk = ~clk;

   logic [255:0] previous_hash = {<<8{256'h718CC2121C3E6410_59BC1C2CFC45666C_99E8AE922F7A807B_7D07B62C995D79E2}};
   logic [ 31:0] response;
   logic [ 63:0] expected_nonce = 64'h2bf29ef0_0786a6bc;
   logic [ 63:0] expected_hash  = 64'hFFFFFFD2_1c3933f4;
   int delay_count=0;

   logic [31:0] awaddr ;
   logic        awvalid;
   logic        awready;

   logic [31:0] wdata  ;
   logic        wvalid ;
   logic        wready ;

   logic [ 1:0] bresp  ;
   logic        bvalid ;
   logic        bready ;

   logic [31:0] araddr ;
   logic        arvalid;
   logic        arready;

   logic [31:0] rdata  ;
   logic [ 1:0] rresp  ;
   logic        rvalid ;
   logic        rready ;

   initial begin
      #100
      @(posedge clk) #0;

      //u_stim.write(8'h08,32'd??); //difficulty
      u_stim.write(8'h1C,32'b0); //don't use the harder difficulty for this test
      for (int i=0;i<8;i++) u_stim.write(8'h20+i*4,previous_hash[32*i+:32]);
      u_stim.write(8'h04,32'h10); //push ififo
      #1;
      u_uut.u_engine.counter = expected_nonce - 64'd150; //make us start 150 cycles before finding the work value
      #1;
      repeat(300) @(posedge clk) #0 delay_count +=1; //need to add enough buffer for the counter to catch up + the pipeline length

      u_stim.read(8'h04,response);
      assert(response[0]) else $error("Output FIFO still empty");
      u_stim.read(8'h60,response);
      assert(response==expected_nonce[0+:32]) else $error("Output FIFO wrong nonce lower");
      u_stim.read(8'h64,response);
      assert(response==expected_nonce[32+:32]) else $error("Output FIFO wrong nonce upper");
      u_stim.read(8'h68,response);
      //assert(response==expected_hash[0+:32]) else $error("Output FIFO wrong hash lower");
      u_stim.read(8'h6C,response);
      assert(response==expected_hash[32+:32]) else $error("Output FIFO wrong hash upper");
      u_stim.write(8'h04,32'h02); //pop ofifo
      u_stim.read(8'h04,response);
      assert(!response[0]) else $error("Output FIFO not empty");

      repeat(10) @(posedge clk) #0;
      $stop;
   end

   sim_axil_master u_stim (
      .clk_i  (clk),
      .awaddr ,
      .awvalid,
      .awready,
      .wdata  ,
      .wvalid ,
      .wready ,
      .bresp  ,
      .bvalid ,
      .bready ,
      .araddr ,
      .arvalid,
      .arready,
      .rdata  ,
      .rresp  ,
      .rvalid ,
      .rready
   );

   conano_axil u_uut (
      .clk_i  (clk),
      .awaddr (awaddr[6:0]),
      .awvalid,
      .awready,
      .wdata  ,
      .wvalid ,
      .wready ,
      .bresp  ,
      .bvalid ,
      .bready ,
      .araddr (araddr[6:0]),
      .arvalid,
      .arready,
      .rdata  ,
      .rresp  ,
      .rvalid ,
      .rready
   );

endmodule
`default_nettype wire
