//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module inferred_fifo#(
   parameter DEPTH_LOG2=10,
   parameter WIDTH     =36
)(
   input uwire logic             clk_i   ,
   input uwire logic [WIDTH-1:0] wdata_i ,
   input uwire logic             pop_i   ,
   input uwire logic             push_i  ,
   output      logic [WIDTH-1:0] rdata_o ,
   output      logic             rvalid_o = 0,
   output      logic             wready_o
);
   logic [WIDTH-1:0] mem [2**DEPTH_LOG2];

   logic [DEPTH_LOG2-1:0] waddr   =0;
   logic [DEPTH_LOG2-1:0] waddr_p1=1;
   logic [DEPTH_LOG2-1:0] raddr   =0;
   logic [DEPTH_LOG2-1:0] raddr_p1=1;

   always @ (posedge clk_i) begin
      //input FIFO
      rdata_o <= mem[raddr];
      if (pop_i && (raddr != waddr)) begin
         raddr    <= raddr_p1;
         raddr_p1 <= raddr_p1+1;
         if (raddr_p1 == waddr) rvalid_o <= 0; //last data word was read
      end

      mem[waddr] <= wdata_i;
      if (push_i && (waddr_p1 != raddr)) begin
         waddr    <= waddr_p1;
         waddr_p1 <= waddr_p1+1;
         rvalid_o <= 1; //potentially overrides
      end
    end
    assign wready_o = (waddr_p1 != raddr);

endmodule
`default_nettype wire
