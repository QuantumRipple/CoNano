//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
//plain verilog wrapper with slight name variations to allow Vivado to include in block designs and infer the correct interface names and clock association.
module conano_axil_verilog_wrapper #(
   parameter TILES=1
)(
   input  wire         s_axil_clk,

   input  wire  [ 6:0] s_axil_awaddr ,
   input  wire         s_axil_awvalid,
   output wire         s_axil_awready,

   input  wire  [31:0] s_axil_wdata  ,
   input  wire         s_axil_wvalid ,
   output wire         s_axil_wready ,

   output wire  [ 1:0] s_axil_bresp  ,
   output wire         s_axil_bvalid ,
   input  wire         s_axil_bready ,

   input  wire  [ 6:0] s_axil_araddr ,
   input  wire         s_axil_arvalid,
   output wire         s_axil_arready,

   output wire  [31:0] s_axil_rdata  ,
   output wire  [ 1:0] s_axil_rresp  ,
   output wire         s_axil_rvalid ,
   input  wire         s_axil_rready
);

   conano_axil #(
      .TILES(TILES)
   ) u_conano(
      .clk_i  (s_axil_clk    ),

      .awaddr (s_axil_awaddr ),
      .awvalid(s_axil_awvalid),
      .awready(s_axil_awready),

      .wdata  (s_axil_wdata  ),
      .wvalid (s_axil_wvalid ),
      .wready (s_axil_wready ),

      .bresp  (s_axil_bresp  ),
      .bvalid (s_axil_bvalid ),
      .bready (s_axil_bready ),

      .araddr (s_axil_araddr ),
      .arvalid(s_axil_arvalid),
      .arready(s_axil_arready),

      .rdata  (s_axil_rdata  ),
      .rresp  (s_axil_rresp  ),
      .rvalid (s_axil_rvalid ),
      .rready (s_axil_rready )
   );

endmodule
`default_nettype wire
