//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module blake2b_mixer_pipe3_stagger (
   input  uwire logic        clk_i,
   input  uwire logic [63:0] ai,bi,cil,dil,xi,yil2,
   output       logic [63:0] ao,bo,col,dol
);
   logic [63:0] a [1:3];
   logic [63:0] b [1:3];
   logic [63:0] c [2:4];
   logic [63:0] d [2:4];
   logic [63:0] an [1:3];
   logic [63:0] bn [1:4];
   logic [63:0] cn [2:4];
   logic [63:0] dn [2:4];
   always_comb begin
      an[1] = ai + bi + xi;
      bn[1] = bi;

      an[2] = a[1];
      dn[2] = {2{dil^a[1]}}>>32;
      cn[2] = cil + dn[1];
      bn[2] = {2{b[1]^cn[2]}}>>24;

      an[3] = a[2]+yil2+b[2];
      dn[3] = {2{d[2]^an[3]}}>>16;
      cn[3] = c[2];
      bn[3] = b[2];

      dn[4] = d[3];
      cn[4] = c[3]+d[3];
      bn[4] = {2{b[3]^cn[4]}}>>63; //comb output to avoid being late, gets put in the path of next a[1], giving it a binary add, ternary add, and xor all in series
   end
   always @ (posedge clk_i) begin
      a[1] <= an[1];
      b[1] <= bn[1];

      a[2] <= an[2];
      b[2] <= bn[2];
      c[2] <= cn[2];
      d[2] <= dn[2];

      a[3] <= an[3];
      b[3] <= bn[3];
      c[3] <= cn[3];
      d[3] <= dn[3];

      c[4] <= cn[4];
      d[4] <= dn[4];
   end
   assign ao  = a[3];
   assign bo  = bn[4];
   assign col = c[4];
   assign dol = d[4];
endmodule
`default_nettype wire
