//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module blake2b_mixer_pipe4 (
   input  uwire logic        clk_i,
   input  uwire logic [63:0] ai,bi,ci,di,xi,yil2,
   output       logic [63:0] ao,bo,co,dou
);
   logic [63:0] a [1:4];
   logic [63:0] b [1:4];
   logic [63:0] c [1:4];
   logic [63:0] d [1:4];
   logic [63:0] an [1:4];
   logic [63:0] bn [1:4];
   logic [63:0] cn [1:4];
   logic [63:0] dn [1:4];
   always_comb begin
      an[1] = ai + bi + xi;

      dn[2] = {2{d[1] ^ a[1]}} >> 32;
      cn[2] = c[1] + dn[2];
      bn[2] = {2{b[1] ^ cn[2]}} >> 24;

      an[3] = a[2]+b[2]+yil2;

      dn[4] = {2{d[3]^a[3]}} >> 16;
      cn[4] = c[3] + dn[4];
      bn[4] = {2{b[3]^cn[4]}} >> 63;
   end
   always @ (posedge clk_i) begin
      a[1] <= an[1];
      b[1] <= bi;
      c[1] <= ci;
      d[1] <= di;

      a[2] <= a[1];
      b[2] <= bn[2];
      c[2] <= cn[2];
      d[2] <= dn[2];

      //second half
      a[3] <= an[3];
      b[3] <= b[2];
      c[3] <= c[2];
      d[3] <= d[2];

      a[4] <= a[3];
      b[4] <= bn[4];
      c[4] <= cn[4];
      d[4] <= dn[4];
   end
   assign ao  = a[4];
   assign bo  = b[4];
   assign co  = c[4];
   assign dou = d[4];
endmodule
`default_nettype wire
