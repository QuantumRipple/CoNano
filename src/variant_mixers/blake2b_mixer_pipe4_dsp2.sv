//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module blake2b_mixer_pipe4_dsp2 ( //variant that uses cregs but no pregs to hopefully reduce route length to the ternary adders
   input  uwire logic        clk_i,
   input  uwire logic [63:0] ai,bi,ci,di,xi,yil2,
   output       logic [63:0] ao,bo,co,dou
);
   logic [63:0] a [1:4];
   logic [63:0] b [1:4];
   logic [63:0] c [1:4];
   logic [63:0] d [1:4];
   logic [63:0] an [1:4];
   logic [63:0] bn [1:4];
   logic [63:0] cn [1:4];
   logic [63:0] dn [1:4];

   logic c2carryn,c4carryn;

   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (1),
      .NUM_CREG  (1),
      .NUM_CIREG (0),
      .NUM_PREG  (0)
   ) u_dsp0 (
      .clk (clk_i       ),
      .abci(48'b0       ),
      .ab  (ci[63:16]   ),
      .abco(            ),
      .c   (dn[1][63:16]),
      .ci  (c2carryn    ),
      .co  (            ),
      .pci (48'b0       ),
      .p   (cn[2][63:16]),
      .pco (            ),
      .opmode(7'b0001111)
   );

   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (1),
      .NUM_CREG  (1),
      .NUM_CIREG (0),
      .NUM_PREG  (0)
   ) u_dsp1 (
      .clk (clk_i       ),
      .abci(48'b0       ),
      .ab  (c[2][63:16] ),
      .abco(            ),
      .c   (dn[3][63:16]),
      .ci  (c4carryn    ),
      .co  (            ),
      .pci (48'b0       ),
      .p   (cn[4][63:16]),
      .pco (            ),
      .opmode(7'b0001111)
   );

   always_comb begin
      an[1] = ai + bi + xi;
      dn[1] = {2{di ^ an[1]}} >> 32;
      cn[1] = ci;
      bn[1] = bi;

      {c2carryn,cn[2][15:0]} = c[1][15:0] + d[1][15:0];
      bn[2] = {2{b[1]^cn[2]}}>>24;
      an[2] = a[1];
      dn[2] = d[1];

      an[3] = a[2]+bn[3]+yil2;
      dn[3] = {2{d[2]^an[3]}}>>16;
      cn[3] = c[2];
      bn[3] = b[2];

      {c4carryn,cn[4][15:0]} = c[3][15:0] + d[3][15:0];
      bn[4] = {2{b[3]^cn[4]}} >> 63;
      an[4] = a[3];
      dn[4] = d[3];
   end
   always @ (posedge clk_i) begin
      a[1] <= an[1];
      b[1] <= bn[1];
      c[1] <= cn[1];
      d[1] <= dn[1];

      a[2] <= an[2];
      b[2] <= bn[2];
      c[2] <= cn[2];
      d[2] <= dn[2];

      //second half
      a[3] <= an[3];
      b[3] <= bn[3];
      c[3] <= cn[3];
      d[3] <= dn[3];

      a[4] <= an[4];
      b[4] <= bn[4];
      c[4] <= cn[4];
      d[4] <= dn[4];
   end
   assign ao  = a[4];
   assign bo  = b[4];
   assign co  = c[4];
   assign dou = d[4];
endmodule
`default_nettype wire
