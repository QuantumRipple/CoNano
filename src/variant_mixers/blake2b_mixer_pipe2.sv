//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module blake2b_mixer_pipe2 (
   input  uwire logic        clk_i,
   input  uwire logic [63:0] ai,bi,ci,di,xi,yil,
   output       logic [63:0] ao,bo,co,dou
);
   (* use_dsp48 = "yes" *) logic [63:0] a [1:2];
   logic [63:0] b [1:2];
   logic [63:0] c [1:2];
   logic [63:0] d [1:2];
   logic [63:0] an [1:2];
   logic [63:0] bn [1:2];
   logic [63:0] cn [1:2];
   logic [63:0] dn [1:2];

   always_comb begin
      an[1] = ai + bi + xi;
      dn[1] = {2{di ^ an[1]}} >> 32;
      cn[1] = ci + dn[1];
      bn[1] = {2{bi ^ cn[1]}} >> 24;
      //second half
      an[2] = a[1]+b[1]+yil;
      dn[2] = {2{d[1]^an[2]}} >> 16;
      cn[2] = c[1] + dn[2];
      bn[2] = {2{b[1]^cn[2]}} >> 63;
   end
   always @ (posedge clk_i) begin
      a[1] <= an[1];
      b[1] <= bn[1];
      c[1] <= cn[1];
      d[1] <= dn[1];
      //second half
      a[2] <= an[2];
      b[2] <= bn[2];
      c[2] <= cn[2];
      d[2] <= dn[2];
   end
   assign ao  = a[2];
   assign bo  = b[2];
   assign co  = c[2];
   assign dou = d[2];
endmodule
`default_nettype wire
