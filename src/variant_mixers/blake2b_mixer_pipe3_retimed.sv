//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module blake2b_mixer_pipe3_retimed (
   input  uwire logic        clk_i,
   input  uwire logic [63:0] ai,bi,ci,di,xi,yil,
   output       logic [63:0] ao,bo,co,dou
);
   logic [63:0] a [1:3];
   (* use_dsp48 = "yes" *) logic [63:0] a2;
   logic [63:0] b [1:3];
   logic [63:0] c [1:3];
   logic [63:0] d [1:3];
   logic [63:0] an [1:3];
   logic [63:0] bn [1:4];
   logic [63:0] cn [1:3];
   logic [63:0] dn [1:3];
   always_comb begin
      an[1] = ai + bi + xi;
      dn[1] = {2{di ^ an[1]}} >> 32;
      cn[1] = ci;
      bn[1] = bi;

      dn[2] = d[1];
      cn[2] = c[1] + d[1];
      bn[2] = {2{b[1] ^ cn[2]}} >> 24;
      an[2] = a[1]+yil;

      an[3] = a2+b[2];
      dn[3] = {2{d[2]^an[3]}} >> 16;
      cn[3] = c[2] + dn[3];
      bn[3] = b[2];

      bn[4] = {2{b[3]^c[3]}} >> 63; //combinational output - shifts the xor into 1st stage of next mixer
   end
   always @ (posedge clk_i) begin
      a[1] <= an[1];
      b[1] <= bn[1];
      c[1] <= cn[1];
      d[1] <= dn[1];

      a2 <= an[2]; //unique name to force DSP use only for this assignment
      b[2] <= bn[2];
      c[2] <= cn[2];
      d[2] <= dn[2];

      //second half
      a[3] <= an[3];
      b[3] <= bn[3];
      c[3] <= cn[3];
      d[3] <= dn[3];
   end
   assign ao  = a[3];
   assign bo  = bn[4];
   assign co  = c[3];
   assign dou = d[3];
endmodule
`default_nettype wire
