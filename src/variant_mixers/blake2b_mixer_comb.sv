//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module blake2b_mixer_comb (
   input  uwire logic        clk_i,
   input  uwire logic [63:0] a,b,c,d,x,y,
   output       logic [63:0] an,bn,cn,dn
);
   logic [63:0] a1,b1,c1,d1,a2,b2,c2,d2;

   assign a1 = a + b + x;
   assign d1 = {2{d ^ a1}} >> 32;
   assign c1 = c + d1;
   assign b1 = {2{b ^ c1}} >> 24;

   assign a2 = a1 + b1 + y;
   assign d2 = {2{d1 ^ a2}} >> 16;
   assign c2 = c1 + d2;
   assign b2 = {2{b1 ^ c2}} >> 63;

   assign an = a2;
   assign bn = b2;
   assign cn = c2;
   assign dn = d2;
endmodule
`default_nettype wire
