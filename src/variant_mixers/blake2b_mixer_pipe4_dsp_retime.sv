//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module blake2b_mixer_pipe4_dsp_retime (
   input  uwire logic        clk_i,
   input  uwire logic [63:0] ai,bi,ci,di,xi,yil2,
   output       logic [63:0] ao,bo,co,dou
);
   logic [63:0] a [1:4];
   logic [63:0] b [1:4];
   logic [63:0] c [1:4];
   logic [63:0] d [1:4];
   logic [63:0] an [1:4];
   logic [63:0] bn [1:5];
   logic [63:0] cn [1:4];
   logic [63:0] dn [1:4];

   logic c2carryn,c4carryn;

   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (1),
      .NUM_CREG  (0),
      .NUM_CIREG (0),
      .NUM_PREG  (1)
   ) u_dsp0 (
      .clk (clk_i       ),
      .abci(48'b0       ),
      .ab  (ci[63:16]   ),
      .abco(            ),
      .c   (dn[2][63:16]),
      .ci  (c2carryn    ),
      .co  (            ),
      .pci (48'b0       ),
      .p   (c[2][63:16] ),
      .pco (            ),
      .opmode(7'b0001111)
   );

   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (1),
      .NUM_CREG  (0),
      .NUM_CIREG (0),
      .NUM_PREG  (1)
   ) u_dsp1 (
      .clk (clk_i       ),
      .abci(48'b0       ),
      .ab  (c[2][63:16] ),
      .abco(            ),
      .c   (dn[4][63:16]),
      .ci  (c4carryn    ),
      .co  (            ),
      .pci (48'b0       ),
      .p   (c[4][63:16] ),
      .pco (            ),
      .opmode(7'b0001111)
   );

   always_comb begin
      an[1] = ai + bi + xi;
      dn[1][15:0] = {2{di^an[1]}} >> 32; //rolls are free, xors are not. only do the xor in this path that's required to avoid having it in the fabric carry chain to DSP.
      dn[1][63:16]= {2{di}} >> (32+16); //shift an extra 16 to adjust for LHS offset
      cn[1] = ci;
      bn[1] = bi;

      dn[2][15: 0] = d[1][15:0] ;
      dn[2][63:16] = d[1][63:16]^({2{a[1]}}>>(32+16)); //A needs to be rolled to match the roll applied to D last cycle
      {c2carryn,cn[2][15:0]} = c[1][15:0] + d[1][15:0];
      bn[2] = b[1];
      an[2] = a[1];

      bn[3] = {2{b[2]^c[2]}}>>24;
      an[3] = a[2]+bn[3]+yil2;
      dn[3][15: 0] = {2{d[2]^an[3]}}>>16;
      dn[3][63:16] = {2{d[2]}}>>(16+16);
      cn[3] = c[2];

      dn[4][15: 0] = d[3][15:0] ;
      dn[4][63:16] = d[3][63:16]^({2{a[3]}}>>(16+16));
      {c4carryn,cn[4][15:0]} = c[3][15:0] + d[3][15:0];
      bn[4] = b[3];
      an[4] = a[3];

      bn[5] = {2{b[4]^c[4]}} >> 63;  //combinatorial output to shift this into the fabric add of the next mixer
   end
   always @ (posedge clk_i) begin
      a[1] <= an[1];
      b[1] <= bn[1];
      c[1] <= cn[1];
      d[1] <= dn[1];

      a[2] <= an[2];
      b[2] <= bn[2];
      c[2][15:0] <= cn[2][15:0];
      d[2] <= dn[2];

      //second half
      a[3] <= an[3];
      b[3] <= bn[3];
      c[3] <= cn[3];
      d[3] <= dn[3];

      a[4] <= an[4];
      b[4] <= bn[4];
      c[4][15:0] <= cn[4][15:0];
      d[4] <= dn[4];
   end
   assign ao  = a[4];
   assign bo  = bn[5];
   assign co  = c[4];
   assign dou = d[4];
endmodule
`default_nettype wire
