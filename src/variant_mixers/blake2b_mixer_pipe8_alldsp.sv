//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module blake2b_mixer_pipe8_alldsp ( //uses DSP blocks for upper 48-bits of all addition operations. Very heavy on DSPs using 6 per mixer, plus long 8-cycle latency. Could be great for high frequency operation on much larger parts though.
   input  uwire logic        clk_i,
   input  uwire logic [63:0] aie,bil,cil,die,xi,yil4,
   output       logic [63:0] aoe,bol,col,doe
);
   logic [15: 0] al [0:7]; //0-7
   logic [15: 0] bl [2:9]; //2-9
   logic [15: 0] cl [2:9]; //2-9
   logic [15: 0] dl [0:7]; //0-7

   logic [63:16] ah [0:7]; //3,7. 2,6 are Pcasc intermediate sums
   logic [63:16] bh [2:9]; //2-9
   logic [63:16] ch [2:9]; //2,5,6,9
   logic [63:16] dh [0:7]; //0-7

   logic [63:0] dc3, bc5, dc7, bc9;

   logic       dspci [0:5];

   //nominally 8 cycles of delay, but a/d is a cycle early and b/c are a cycle late
   //y enters 4 cycles late
   always @ (posedge clk_i) begin
      //16 bit wide lower fabric add/carry gen
      al[0] <= aie[15:0];
      {dspci[0],al[1]} <= 17'( xi[15:0]+al[0]);//lat1
      {dspci[1],al[2]} <= 17'(bil[15:0]+al[1]);//lat2

      al[3] <= al[2];
      dl[3] <= al[2] ^ dl[2]; //not rolled yet

      dl[0:2] <= {die[15:0],dl[0:1]}; //3 deep

      cl[2:3] <= {cil[15:0],cl[2]}; //2 deep
      {dspci[2],cl[4]} <= 17'(dc3[15:0]+cl[3]); //lat4

      cl[5]   <= cl[4];
      bl[2:4] <= {bil[15:0],bl[2:3]}; //3 deep
      bl[5]   <= cl[4] ^ bl[4]; //not rolled yet

      al[4] <= al[3];
      {dspci[3],al[5]} <= 17'(yil4[15:0]+al[4]); //lat5
      {dspci[4],al[6]} <= 17'(bc5[15:0] +al[5]); //lat6
      al[7] <= al[6];

      dl[4:6] <= {dc3[15:0],dl[4:5]}; //3 deep
      dl[7]   <= al[6] ^ dl[6]; //not rolled yet

      cl[6:7]  <= cl[5:6]; //2 deep
      {dspci[5],cl[8]} <= 17'(dc7[15:0]+cl[7]); //lat8
      cl[9]    <= cl[8];
      bl[6:8]  <= {bc5[15:0],bl[6:7]};
      bl[9]    <= cl[8] ^ bl[8]; //not rolled yet


      //fabric registers between the upper DSP segments:
      dh[0:3] <= {die[63:16],dh[0:2]}; //4 deep shreg
      bh[2:5] <= {bil[63:14],bh[2:4]}; //4 deep shreg
      dh[4:7] <= {dc3[63:16],dh[4:6]}; //4 deep shreg
      bh[6:9] <= {bc5[63:16],bh[6:8]}; //4 deep shreg
   end

   always_comb begin
      //combinational "roll" operations, actually minimal logic because it's only a 48-bit bitwise XOR
      dc3 = {2{{ah[3]^dh[3],dl[3]}}}>>32;
      bc5 = {2{{ch[5]^bh[5],bl[5]}}}>>24;
      dc7 = {2{{ah[7]^dh[7],dl[7]}}}>>16;
      bc9 = {2{{ch[9]^bh[9],bl[9]}}}>>63;
   end

   //48 bit upper DSP sections
   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (2),
      .NUM_CIREG (0)
   ) u_dsp0 (
      .clk (clk_i     ),
      .abci(48'b0     ),
      .ab  (aie[63:16]),
      //ab2 <= ah[0]
      .abco(          ),
      .c   (xi        ),
      .cin (dspci[0]  ), //carry(xi[15:0]+al[0])
      .pci (48'b0     ),
      .p   (          ), //a1+x1
      .pco (ah[2]     ),
      .opmode(7'b0001111)
   );

   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (1),
      .NUM_CIREG (0)
   ) u_dsp1 (
      .clk (clk_i     ),
      .abci(48'b0     ),
      .ab  (cil[63:16]),
      .abco(ch[2]    ),
      .c   (bil[63:16]),
      .cin (dspci[1]  ), //carry(bil[15:0]+dsplo[0])
      .pci (ah[2]     ),
      .p   (ah[3]     ), //b[2] + cascade_from_dsp0p
      .pco (          ),
      .opmode(7'b0011100)
   );

   dsp48_wrap #(
      .AB_CASCADE(1),
      .NUM_ABREG (2),
      .NUM_CIREG (0)
   ) u_dsp2 (
      .clk (clk_i     ),
      .abci(ch[2]     ),
      .ab  (48'b0     ),
      // ab2 <= ch[3]
      .abco(          ),
      .c   (dc3[63:16]),
      .cin (dspci[2]  ), //carry(dc3[15:0]+cl[3])
      .pci (48'b0     ),
      .p   (ch[5]     ), //dh[4]+ch[4]
      .pco (          ),
      .opmode(7'b0001111)
   );

   //second half
   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (2),
      .NUM_CIREG (0)
   ) u_dsp3 (
      .clk (clk_i     ),
      .abci(48'b0      ),
      .ab  (ah[3]      ),
      // ab2 <= ah[4]
      .abco(           ),
      .c   (yil4[63:16]),
      .cin (dspci[3]   ), //carry(yil4[15:0]+al[4])
      .pci (48'b0      ),
      .p   (           ), //ah[5]+yh[5]
      .pco (ah[6]      ),
      .opmode(7'b0001111)
   );

   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (1),
      .NUM_CIREG (0)
   ) u_dsp4 (
      .clk (clk_i     ),
      .abci(48'b0     ),
      .ab  (ch[5]     ),
      .abco(ch[6]     ),
      .c   (bc5[63:16]),
      .cin (dspci[4]  ), //carry(bc5[15:0]+dsplo[3])
      .pci (ah[6]     ),
      .p   (ah[7]     ), //bh[6]+cascade_from_dsp3p
      .pco (          ),
      .opmode(7'b0011100)
   );


   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (2),
      .NUM_CIREG (0)
   ) u_dsp5 (
      .clk (clk_i     ),
      .abci(ch[6]     ),
      .ab  (4'b0      ),
      //ab2 <= ch[7]
      .abco(          ),
      .c   (dc7[63:16]),
      .cin (dspci[5]  ), //carry(dc7[15:0]+cl[7])
      .pci (48'b0     ),
      .p   (ch[9]     ), //dh[8]+ch[8]
      .pco (          ),
      .opmode(7'b0001111)
   );

   assign aoe = {ah[7],al[7]};
   assign bol = bc9;
   assign col = {ch[9],cl[9]};
   assign doe = dc7;


   //dsp5< //takes abcascade (1layer) from dsp4
   //dsp4 ** //takes pcascade from dsp3
   //dsp3 //fabric P from DSP1

   //dsp2< //takes abcascade (1 layer) from dsp1
   //dsp1 ** //takes pcascade from dsp0
   //dsp0 //fabric P from previous DSP4

endmodule
`default_nettype wire
