//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module blake2b_mixer_pipe8 (
   input  uwire logic        clk_i,
   input  uwire logic [63:0] ai,bi,ci,di,xi,yi,
   output       logic [63:0] an,bn,cn,dn
);
   logic [63:0] a [1:8];
   logic [63:0] b [1:8];
   logic [63:0] c [1:8];
   logic [63:0] d [1:8];
   logic [63:0] y [1:4];

   always @ (posedge clk_i) begin
      a[1] <= ai + bi + xi;
      b[1] <= bi;
      c[1] <= ci;
      d[1] <= di;
      y[1] <= yi;

      a[2] <= a[1];
      b[2] <= b[1];
      c[2] <= c[1];
      d[2] <= {2{d[1] ^ a[1]}} >> 32;
      y[2] <= y[1];

      a[3] <= a[2];
      b[3] <= b[2];
      c[3] <= c[2] + d[2];
      d[3] <= d[2];
      y[3] <= y[2];

      a[4] <= a[3];
      b[4] <= {2{b[3] ^ c[3]}} >> 24;
      c[4] <= c[3];
      d[4] <= d[4];
      y[4] <= y[3];

      //second half
      a[5] <= a[4] + b[4] + y[4];
      b[5] <= b[4];
      c[5] <= c[4];
      d[5] <= d[4];

      a[6] <= a[5];
      b[6] <= b[5];
      c[6] <= c[5];
      d[6] <= {2{d[5] ^ a[5]}} >> 16;

      a[7] <= a[6];
      b[7] <= b[6];
      c[7] <= c[6] + d[6];
      d[7] <= d[6];

      a[8] <= a[7];
      b[8] <= {2{b[7] ^ c[7]}} >> 63;
      c[8] <= c[7];
      d[8] <= d[7];
   end

   assign an = a[8];
   assign bn = b[8];
   assign cn = c[8];
   assign dn = d[8];

endmodule
`default_nettype wire
