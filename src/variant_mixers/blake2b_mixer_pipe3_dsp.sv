//Copyright (c) 2021 QuantumRipple
//MIT licensed

`timescale 1ns / 1ps
`default_nettype none
module blake2b_mixer_pipe3_dsp (
   input  uwire logic        clk_i,
   input  uwire logic [63:0] ai,bi,ci,di,xi,yi,
   output       logic [63:0] ao,bo,co,dou
);
   logic [63:0] a [1:3];
   logic [63:0] b [1:3];
   logic [63:0] c [1:3];
   logic [63:0] d [1:3];
   logic [63:0] an [1:3];
   logic [63:0] bn [1:3];
   logic [63:0] cn [1:3];
   logic [63:0] dn [1:3];
   logic c1carry;
   logic [63:0] y [1:1];

   dsp48_wrap #(
      .AB_CASCADE(0),
      .NUM_ABREG (0),
      .NUM_CREG  (0),
      .NUM_CIREG (0),
      .NUM_PREG  (1)
   ) u_dsp0 (
      .clk (clk_i      ),
      .abci(48'b0      ),
      .ab  (ci[47:0]   ),
      .abco(           ),
      .c   (dn[1][47:0]),
      .ci  (1'b0       ),
      .co  (c1carry    ),
      .pci (48'b0      ),
      .p   (c[1][47:0] ),
      .pco (           ),
      .opmode(7'b0001111)
   );

   always_comb begin
      an[1] = ai + bi + xi;
      dn[1] = {2{di^an[1]}}>>32;
      cn[1][63:48] = ci[48:32];
      bn[1] = bi;

      cn[2][47: 0] = c[1][47: 0];
      cn[2][63:32] = c[1][63:48] + d[1][63:48] + c1carry;
      bn[2] = {2{b[1]^cn[2]}}>>24;
      an[2] = a[1]+y[1]+bn[2];
      dn[2] = d[1];

      an[3] = a[2];
      dn[3] = {2{d[2]^a[2]}}>>16;
      cn[3] = c[2] + dn[3];
      bn[3] = {2{b[2]^cn[3]}}>>63;
   end
   always @ (posedge clk_i) begin
      a[1] <= an[1];
      b[1] <= bn[1];
      c[1][63:48] <= cn[1][63:48];
      d[1] <= dn[1];
      y[1] <= yi;

      a[2] <= an[2];
      b[2] <= bn[2];
      c[2] <= cn[2];
      d[2] <= dn[2];

      a[3] <= an[3];
      b[3] <= bn[3];
      c[3] <= cn[3];
      d[3] <= dn[3];
   end
   assign ao  = a[3];
   assign bo  = b[3];
   assign co  = c[3];
   assign dou = d[3];
endmodule
`default_nettype wire
