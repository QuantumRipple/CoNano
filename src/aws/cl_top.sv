// Amazon FPGA Hardware Development Kit
//
// Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License"). You may not use
// this file except in compliance with the License. A copy of the License is
// located at
//
//    http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is distributed on
// an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
// implied. See the License for the specific language governing permissions and
// limitations under the License.

// Xilinx template design file for Amazon FPGA Hardware Development Kit
//
// File: cl_top.sv
// Starting-point top-level HDL Custom Logic (CL) design file
// If all custom logic is contained within the cl.bd block diagram,
//   then no further changes are required to this cl_top file.

module cl_top (
  // Do not modify the interface of this module.
  // User-defined custom logic cannot create additional external ports.
  `include "cl_ports_hlx.vh"
  );
     logic clk;

   logic  [31:0] araddr;
   logic         arready;
   logic         arvalid;

   logic  [31:0] awaddr;
   logic         awready;
   logic         awvalid;

   logic         bready;
   logic  [1:0]  bresp;
   logic         bvalid;

   logic  [31:0] rdata;
   logic         rready;
   logic  [1:0]  rresp;
   logic         rvalid;

   logic  [31:0] wdata;
   logic         wready;
   logic         wvalid;


  // Instantiate the Xilinx Vivado IP Integrator Block Diagram.
  cl cl_inst (
    // Users optionally add new BD external port connections here.
    // Terminate user-defined port connections list with a trailing comma.
          .M_AXI_0_araddr (araddr ),
      .M_AXI_0_arready(arready),
      .M_AXI_0_arvalid(arvalid),
      .M_AXI_0_awaddr (awaddr ),
      .M_AXI_0_awready(awready),
      .M_AXI_0_awvalid(awvalid),
      .M_AXI_0_bready (bready ),
      .M_AXI_0_bresp  (bresp  ),
      .M_AXI_0_bvalid (bvalid ),
      .M_AXI_0_rdata  (rdata  ),
      .M_AXI_0_rready (rready ),
      .M_AXI_0_rresp  (rresp  ),
      .M_AXI_0_rvalid (rvalid ),
      .M_AXI_0_wdata  (wdata  ),
      .M_AXI_0_wready (wready ),
      .M_AXI_0_wstrb  (       ), //unused
      .M_AXI_0_wvalid (wvalid ),
      .clk_o          (clk    ),

    // Do not remove the following mandatory port connections.
    `include "sh_connectors.vh"
    );
    
  // Users optionally add custom RTL logic here.
 nano_blake2b_axil_wrapper #(
    .TILES(10)
 ) u_nano_blake2b(
      .clk_i  (clk),

      .awaddr (awaddr[6:2]),
      .awvalid,
      .awready,

      .wdata  ,
      .wvalid ,
      .wready ,

      .bresp  ,
      .bvalid ,
      .bready ,

      .araddr (araddr[6:2]),
      .arvalid,
      .arready,

      .rdata  ,
      .rresp  ,
      .rvalid ,
      .rready
   );

endmodule    
