
# Constants determined by the FPGA IP Core
REVISION_INFO = 0x00

# Commands, response control, and status to process previous_hashS
FIFO_STATUS_CONTROL = 0x04

# Difficulty thresholds
DIFFICULTY_THRESHOLDS = 0x08

# Commands for stopping, starting, and clearing PoW queue
SYSTEM_CONTROL = 0x0C

# 1-bit difficulty selection index (send 1, receive 0)
DIFFICULTY_SELECTION = 0x1C

# 256-bit input previous_hash spread across 8 32-bit registers
INPUT_PREVIOUS_HASH_ZERO = 0x20
INPUT_PREVIOUS_HASH_ONE = 0x24
INPUT_PREVIOUS_HASH_TWO = 0x28
INPUT_PREVIOUS_HASH_THREE = 0x2C
INPUT_PREVIOUS_HASH_FOUR = 0x30
INPUT_PREVIOUS_HASH_FIVE = 0x34
INPUT_PREVIOUS_HASH_SIX = 0x38
INPUT_PREVIOUS_HASH_SEVEN = 0x3C

# 256-bit out previous_hash spread across 8 32-bit registers
OUTPUT_PREVIOUS_HASH_ZERO = 0x40
OUTPUT_PREVIOUS_HASH_ONE = 0x44
OUTPUT_PREVIOUS_HASH_TWO = 0x48
OUTPUT_PREVIOUS_HASH_THREE = 0x4C
OUTPUT_PREVIOUS_HASH_FOUR = 0x50
OUTPUT_PREVIOUS_HASH_FIVE = 0x54
OUTPUT_PREVIOUS_HASH_SIX = 0x58
OUTPUT_PREVIOUS_HASH_SEVEN = 0x5C

# 64-bit work nounce spread across 2 32-bit registers
OUTPUT_WORK_NOUNCE_ONE = 0x60
OUTPUT_WORK_NOUNCE_TWO = 0x64

# 64-bit work value (hash) spread across 2 32-bit registers
OUTPUT_WORK_VALUE_ONE = 0x68
OUTPUT_WORK_VALUE_TWO = 0x6C
