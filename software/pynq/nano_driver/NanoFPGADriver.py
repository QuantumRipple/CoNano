from pynq import DefaultIP
from bitstring import BitArray

import NanoDriverConsts
from ctypes import c_uint

import numpy as np


class NanoFPGADriver(DefaultIP):
    def __init__(self, description):
        super().__init__(description=description)
        
    # bind to nano pow core
    bindto = ['xilinx.com:module_ref:conano_axil_verilog_wrapper:1.0']

    def get_revision_info(self):
        return self.read(NanoDriverConsts.REVISION_INFO)

    def get_output_previous_hash(self):
        return [self.read(NanoDriverConsts.OUTPUT_PREVIOUS_HASH_ZERO),
        		self.read(NanoDriverConsts.OUTPUT_PREVIOUS_HASH_ONE),
        		self.read(NanoDriverConsts.OUTPUT_PREVIOUS_HASH_TWO),
        		self.read(NanoDriverConsts.OUTPUT_PREVIOUS_HASH_THREE),
        		self.read(NanoDriverConsts.OUTPUT_PREVIOUS_HASH_FOUR),
        		self.read(NanoDriverConsts.OUTPUT_PREVIOUS_HASH_FIVE),
        		self.read(NanoDriverConsts.OUTPUT_PREVIOUS_HASH_SIX),
         		self.read(NanoDriverConsts.OUTPUT_PREVIOUS_HASH_SEVEN)]

    def get_output_work_value(self):
        return [self.read(NanoDriverConsts.OUTPUT_WORK_VALUE_ONE),
        		self.read(NanoDriverConsts.OUTPUT_WORK_VALUE_TWO)]
    
    def get_output_work_nounce(self):
        return [self.read(NanoDriverConsts.OUTPUT_WORK_NOUNCE_ONE),
        		self.read(NanoDriverConsts.OUTPUT_WORK_NOUNCE_TWO)]
    
    def pop_output(self):
      status_control = np.uint32(1<<1)
      self.write_uint(NanoDriverConsts.FIFO_STATUS_CONTROL, status_control)

    def push_input(self):
      status_control = np.uint32(1<<4)
      self.write_uint(NanoDriverConsts.FIFO_STATUS_CONTROL, status_control)

    def check_push_input_ready(self):
      value = self.read(NanoDriverConsts.FIFO_STATUS_CONTROL)
      return self.get_bits(value, 4, 4)
    
    def input_previous_hash(self, previous_hash):
      self.write_uint(NanoDriverConsts.INPUT_PREVIOUS_HASH_ZERO, previous_hash[0])
      self.write_uint(NanoDriverConsts.INPUT_PREVIOUS_HASH_ONE, previous_hash[1])
      self.write_uint(NanoDriverConsts.INPUT_PREVIOUS_HASH_TWO, previous_hash[2])
      self.write_uint(NanoDriverConsts.INPUT_PREVIOUS_HASH_THREE, previous_hash[3])
      self.write_uint(NanoDriverConsts.INPUT_PREVIOUS_HASH_FOUR, previous_hash[4])
      self.write_uint(NanoDriverConsts.INPUT_PREVIOUS_HASH_FIVE, previous_hash[5])
      self.write_uint(NanoDriverConsts.INPUT_PREVIOUS_HASH_SIX, previous_hash[6])
      self.write_uint(NanoDriverConsts.INPUT_PREVIOUS_HASH_SEVEN, previous_hash[7])

    def check_pow_ready(self):
      fifo_status = self.read(NanoDriverConsts.FIFO_STATUS_CONTROL)
      return fifo_status

    def get_revision_info(self):
      return self.read(NanoDriverConsts.REVISION_INFO)

    def get_pow_type(self):
      return self.read(NanoDriverConsts.DIFFICULTY_SELECTION)

    def dump(self):
      system_control_registor = self.read(NanoDriverConsts.SYSTEM_CONTROL)
      system_control_registor = self.set_bits(system_control_registor, 1, 1, 1)
      self.write_uint(NanoDriverConsts.SYSTEM_CONTROL, system_control_registor)

    def stop_dump(self):
      system_control_registor = self.read(NanoDriverConsts.SYSTEM_CONTROL) 
      system_control_registor = self.set_bits(system_control_registor, 0, 1, 1) 

      self.write_uint(NanoDriverConsts.SYSTEM_CONTROL, system_control_registor) 

    def abort_work(self):
      system_control_registor = self.read(NanoDriverConsts.SYSTEM_CONTROL) 
      system_control_registor = self.set_bits(system_control_registor, 1, 0, 0) 

      self.write_uint(NanoDriverConsts.SYSTEM_CONTROL, system_control_registor) 

    def disable(self):
      system_control_registor = self.read(NanoDriverConsts.SYSTEM_CONTROL) 
      system_control_registor = self.set_bits(system_control_registor, 1, 2, 2) 

      self.write_uint(NanoDriverConsts.SYSTEM_CONTROL, system_control_registor) 

    def enable(self):
      system_control_registor = self.read(NanoDriverConsts.SYSTEM_CONTROL) 
      system_control_registor = self.set_bits(system_control_registor, 0, 2, 2) 

      self.write_uint(NanoDriverConsts.SYSTEM_CONTROL, system_control_registor) 

    def set_pow_to_receive(self):
      self.write_uint(NanoDriverConsts.DIFFICULTY_SELECTION, 0) 

    def set_pow_to_send(self):
      self.write_uint(NanoDriverConsts.DIFFICULTY_SELECTION, 1) 

    def set_receive_threshold(self, threshold):
      diff_register = self.read(NanoDriverConsts.DIFFICULTY_THRESHOLDS) 
      diff_register = self.set_bits(diff_register, threshold, 0, 5) 

      self.write_uint(NanoDriverConsts.DIFFICULTY_THRESHOLDS, diff_register) 

    def set_send_threshold(self, threshold):
      diff_register = self.read(NanoDriverConsts.DIFFICULTY_THRESHOLDS)
      diff_register = self.set_bits(diff_register, threshold, 8, 13) 

      self.write_uint(NanoDriverConsts.DIFFICULTY_THRESHOLDS, diff_register) 

    def get_receive_threshold(self):
      value = self.read(NanoDriverConsts.DIFFICULTY_THRESHOLDS) 
      return get_bits(value, 0, 5)

    def get_send_threshold(self):
      value = self.read(NanoDriverConsts.DIFFICULTY_THRESHOLDS)
      return get_bits(value, 8, 13)

    def write_uint(self, register_offset, data):
       self.mmio.array[register_offset >> 2] = np.uint32(data)

    def get_bits(self, input_value, start_bit, end_bit):
      input_value = np.uint32(input_value)
      mask = np.uint32((np.uint32(1) << (start_bit - end_bit + 1)) - 1)
      return np.uint32((input_value >> start_bit) & mask)

    def set_bits(self, orig_value, to_set, start_bit, end_bit):
      to_set = np.uint32(to_set)
      all_ones = np.uint32(2**32 - 1)

      left = np.uint32(all_ones << (start_bit))
      right = ((np.uint32(1) << end_bit)) - 1
      mask = np.uint32(~(left & right))

      masked_n = np.uint32(orig_value & mask)
      m_shifted = np.uint32(to_set << start_bit)

      return np.uint32(masked_n | m_shifted)

    def dump_mem(self):
      for z in [hex(x)[2:].zfill(8) for x in self.mmio.array]:
        print(z)
