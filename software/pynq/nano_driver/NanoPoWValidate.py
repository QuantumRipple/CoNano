from NanoPoWWrapper import input_hash, check_pow_output_ready, get_pow_output, clear_pow_queue
from NanoFPGAOverlay import NanoFPGAOverlay

from nanolib.work import validate_work, get_work_value, blake2b
from binascii import hexlify, unhexlify

def nano_fpga_validate():
    print("== RUNNING nano_fpga_validate ==") 

    print("== RUNNING SEND POW ==") 
    fpga_overlay = NanoFPGAOverlay("/home/xilinx/pynq/overlays/nano/conano_pynqz2_slow.bit")
    driver = fpga_overlay.nano_driver

    # clear any remaining pow in queue
    clear_pow_queue(driver)

    # # set to generate pow at send threshold
    driver.set_pow_to_send() 
    driver.set_send_threshold(35) # 35

    # # set some bits in previous_hash and send for work generation
    previous_hash = [1, 0, 0, 0, 0, 0, 0, 0]
    input_hash(driver, previous_hash) 
    check_pow_output_ready(driver) 

    # # # get work and pop from queue
    output_previous_hash, work_value, nonce_value = get_pow_output(driver) 
    driver.pop_output()
    print("previous_hash send:")
    [print(hex(i)) for i in output_previous_hash]

    print("nonce_value:") 
    [print(hex(i)) for i in nonce_value]

    print("work_value:")
    [print(hex(i)) for i in work_value]

    output_previous_hash.reverse()
    nonce_value.reverse()
    work_value.reverse()

    print("previous_hash reverse send:")
    [print(hex(i)) for i in output_previous_hash]

    print("nonce_value reverse:") 
    [print(hex(i)) for i in nonce_value]

    print("work_value reverse:") 
    [print(hex(i)) for i in work_value]
    test_block = ''.join('{:08X}'.format(x) for x in output_previous_hash)
    test_nonce = ''.join('{:08X}'.format(x) for x in nonce_value)

    print("test_block", test_block)
    print("test_nonce", test_nonce)
    driver.dump_mem()

    reversed_work = bytearray(unhexlify("5b064dcc70b9db0a"))
    reversed_work.reverse()
    work_hash = bytearray(blake2b(b"".join([reversed_work, unhexlify("B585D9363B8265CFD5993F30A3D6DE6B5CA5CC7879E0AFA94D13F08B713B9FFD")]), digest_size=8).digest())
    work_hash.reverse()
    work_value = int(hexlify(work_hash), 16)
    work_value = dec_to_hex(work_value, 8).lower()
    print("work_value", work_value)
    print("== RUNNING RECEIVE POW ==")
    # set to generate pow at receive threshold
    driver.set_pow_to_receive() 
    driver.set_receive_threshold(41) 

    input_hash(driver, previous_hash) 

    # pull until pow ready
    check_pow_output_ready(driver) 

    # get work and pop queue
    output_previous_hash, work_value, nonce_value = get_pow_output(driver) 
    driver.pop_output()

    print("previous_hash send:")
    [print(hex(i)) for i in output_previous_hash]

    print("nonce_value:")
    [print(hex(i)) for i in nonce_value]

    print("work_value:")
    [print(hex(i)) for i in work_value]

    print("fpga_validate is working as expected")
    print("== PASSED ==")

def dec_to_hex(d, n):
    return format(d, "0{}X".format(n*2))

if __name__ == "__main__":
    nano_fpga_validate()
