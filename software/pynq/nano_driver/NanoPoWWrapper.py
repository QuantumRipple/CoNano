import NanoFPGADriver
import time

def input_hash(driver, previous_hash):
    queue_not_ready_to_accept_input = driver.check_push_input_ready()
    while queue_not_ready_to_accept_input:
        time.sleep(0.01)
        queue_not_ready_to_accept_input = driver.check_push_input_ready()

    driver.input_previous_hash(previous_hash)
    driver.push_input()

    queue_processing_input = driver.check_push_input_ready()
    while queue_processing_input:
        time.sleep(0.01)
        queue_processing_input = driver.check_push_input_ready()

def check_pow_output_ready(driver):
    pow_ready = driver.check_pow_ready()
    while not pow_ready:
        time.sleep(0.01)
        pow_ready = driver.check_pow_ready()


def get_pow_output(driver):
    output_previous_hash = driver.get_output_previous_hash() 
    work_value = driver.get_output_work_value() 
    nonce_value = driver.get_output_work_nounce() 

    return (output_previous_hash, work_value, nonce_value)

def clear_pow_queue(driver):
    pow_ready = driver.check_pow_ready()
    while pow_ready:
        driver.pop_output()
        time.sleep(0.01)
        pow_ready = driver.check_pow_ready()
