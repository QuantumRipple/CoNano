import time

import NanoPowWrapper

if __name__ == "__main__":
    nano_fpga_benchmark()

def nano_fpga_benchmark():
    printf("== RUNNING nano_fpga_benchmark ==\n") 

    printf("== GENERATING AND VALIDATING SEND POW ==\n") 
    previous_hash = bytes([0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000])

    fpga_overlay = NanoFPGAOverlay("TODO/FILENAME")
    driver = fpga_overlay.nano_driver

    # clear any remaining pow in queue
    clear_pow_queue(driver)

    # benchmark send for 10 PoW
    benchmark_number_send = 10
    items_in_queue = 0
    start_time_send = round(time.time() * 1000)
    for i in range(benchmark_number_send):
        # set to generate pow at send threshold
        driver.set_pow_to_send() 
        driver.set_send_threshold(35) 

        # set some bits in previous_hash and send for work generation
        previous_hash[0] = 0x0000 + i
        input_hash(driver, previous_hash) 
        items_in_queue++

    while items_in_queue > 0:
        # pull until pow ready
        check_pow_output_ready(driver) 

        # get work and pop from queue
        output_previous_hash, work_value, nonce_value = get_pow_output(driver) 
        driver.pop_output()
        items_in_queue--

    send_time_send = round(time.time() * 1000) 

    printf("== GENERATING AND VALIDATING RECEIVE POW ==\n") 

    start_time_receive = round(time.time() * 1000)
    benchmark_number_receive = 50
    
    # benchmark receive for 50 PoW (receive is about x64 easier then send)
    for i in range(benchmark_number_receive):
        # set to generate pow at receive threshold
        driver.set_pow_to_receive() 
        driver.set_receive_threshold(41) 

        # set some bits in previous_hash and send for work generation
        previous_hash[0] = 0x0000 + i
        input_hash(driver, previous_hash) 
        items_in_queue++

    while items_in_queue > 0:
        # pull until pow ready
        check_pow_output_ready(driver) 

        # get work and pop from queue
        output_previous_hash, work_value, nonce_value = get_pow_output(driver) 
        driver.pop_output() 
        items_in_queue-- 

    end_time_receive = round(time.time() * 1000) 

    send_total_time = (end_time_send - start_time_send)/1000.0
    receive_total_time = (end_time_receive - start_time_receive)/1000.0

    print("== SINGLE FPGA %.4f SEND POW PER SECOND ==\n", benchmark_number_send / send_total_time) 
    print("== TOTAL GENERATION TIME %.4f SECONDS FOR %d SEND POW ==\n", send_total_time, benchmark_number_send) 

    print("== SINGLE FPGA %.4f RECEIVE POW PER SECOND==\n", benchmark_number_receive / receive_total_time) 
    print("== TOTAL GENERATION TIME %.4f SECONDS FOR %d RECEIVE POW ==\n", receive_total_time, benchmark_number_receive) 
