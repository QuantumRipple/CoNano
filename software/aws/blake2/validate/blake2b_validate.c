#include <stdio.h>
#include <assert.h>

#include "../base/blake2.h"


int main()
{

   printf("== RUNNING blake2_validate ==\n");
   uint8_t pre_computed_nonce[8] = {
      0x2b, 0xf2, 0x9e, 0xf0, 0x07, 0x86, 0xa6, 0xbc
    };

   uint8_t previous_hash [32] = {
      0xE2, 0x79, 0x5D, 0x99, 0x2C, 0xB6, 0x07, 0x7D,
      0x7B, 0x80, 0x7A, 0x2F, 0x92, 0xAE, 0xE8, 0x99,
      0x6C, 0x66, 0x45, 0xFC, 0x2C, 0x1C, 0xBC, 0x59,
      0x10, 0x64, 0x3E, 0x1C, 0x12, 0xC2, 0x8C, 0x71
   };

   // concatenate nonce + hash
   uint8_t hash_nounce_previous_hash[40];
   size_t hash_nounce_previous_hash_index = 0;
   int i;

   for (i = 7; i >= 0; i--)
   {
      hash_nounce_previous_hash[hash_nounce_previous_hash_index] = pre_computed_nonce[i];
      hash_nounce_previous_hash_index++;
   }

   for (i = 31; i >= 0; i--)
   {
      hash_nounce_previous_hash[hash_nounce_previous_hash_index] = previous_hash[i];
      hash_nounce_previous_hash_index++;
   }

   // output from blake2b C computation
   uint8_t hash_output_c[8] = {
      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
   };

   blake2b(&hash_output_c, 8, &hash_nounce_previous_hash, 40, NULL, 0);

   uint8_t expected_outout[8] = {
      0xf4, 0x33, 0x39, 0x1c, 0xd2, 0xff, 0xff, 0xff 
   };

   // validate hash from C library is expected value
   assert(memcmp(&hash_output_c, &expected_outout, 8*sizeof(uint8_t)) == 0);

   printf("blake2b C implementation is working as expected\n");
   printf("== PASSED ==\n");

   return 0;
}