#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include <fpga_pci.h>
#include <fpga_mgmt.h>
#include <utils/lcd.h>

#include <utils/sh_dpi_tasks.h>

#include "../fpga_nano_lib/base/fpga_nano_pow.h"
#include "../fpga_nano_lib/wrapper/generate_work.h"
#include "../blake2/base/blake2.h"


const struct logger *logger = &logger_stdout;

int main(int argc, char **argv)
{
    printf("== RUNNING single_fpga_validate ==\n");

    printf("== RUNNING SEND POW ==\n");
    pci_bar_handle_t pci_bar_handle = PCI_BAR_HANDLE_INIT;

    uint32_t *previous_hash = (uint32_t*)calloc(8, sizeof(uint32_t));

    // single FPGA will be put into slot '0'
    int rc = setup_fpga_connection(&pci_bar_handle, 0 /* slot */);
    fail_on(rc, err, "Cannot setup FPGA connection\n");

    // clear any remaining pow in queue
    clear_pow_queue(pci_bar_handle);

    // set to generate pow at send threshold
    rc = set_pow_to_send(&pci_bar_handle);
    fail_on(rc, err, "Cannot set_pow_to_send\n");

    rc = set_send_threshold(&pci_bar_handle, 35);
    fail_on(rc, err, "Cannot set_send_threshold\n");

    // set some bits in previous_hash and send for work generation
    previous_hash[0] = (uint32_t)2;
    input_hash(pci_bar_handle, previous_hash);

    // pull until pow ready
    check_pow_output_ready(pci_bar_handle);

    // get work and pop from queue
    struct work_t work;
    get_pow_output(pci_bar_handle, &work);
    pop_pow(pci_bar_handle);

    printf("previous_hash send:\n");
    int i;
    for(i = 0; i < 8; i++)
    {
         print_binary(work.previous_hash[i]);
    }

    printf("nonce_value:\n");
    print_binary(work.nonce_value[0]);
    print_binary(work.nonce_value[1]);

    printf("work_value:\n");
    print_binary(work.work_value[0]);
    print_binary(work.work_value[1]);

    printf("== VALIDATING AGAINST BLAKE2B IN C ==\n");
    validate_pow(&work);
    printf("== VALIDATING SEND POW PASSED ==\n");

    printf("== RUNNING RECEIVE POW ==\n");
    // set to generate pow at receive threshold
    rc = set_pow_to_receive(&pci_bar_handle);
    fail_on(rc, err, "Cannot set_pow_to_receive\n");

    rc = set_receive_threshold(&pci_bar_handle, 41);
    fail_on(rc, err, "Cannot set_receive_threshold\n");

    previous_hash[0] = (uint32_t)4;
    input_hash(pci_bar_handle, previous_hash);

    // pull until pow ready
    check_pow_output_ready(pci_bar_handle);

    // get work and pop queue
    get_pow_output(pci_bar_handle, &work);
    pop_pow(pci_bar_handle);

    printf("previous_hash receive:\n");
    for(i = 0; i < 8; i++)
    {
         print_binary(work.previous_hash[i]);
    }

    printf("nonce_value:\n");
    print_binary(work.nonce_value[0]);
    print_binary(work.nonce_value[1]);

    printf("work_value:\n");
    print_binary(work.work_value[0]);
    print_binary(work.work_value[1]);

    printf("== VALIDATING AGAINST BLAKE2B IN C ==\n");
    validate_pow(&work);
    printf("== VALIDATED RECEIVE POW ==\n");

    printf("single_fpga_validate is working as expected\n");
    printf("== PASSED ==\n");

    clean_up_connection(pci_bar_handle);

    free(work.previous_hash);
    free(work.work_value);
    free(work.nonce_value);
    free(previous_hash);
   return rc;
   err:
     clean_up_connection(pci_bar_handle);

     free(work.previous_hash);
     free(work.work_value);
     free(work.nonce_value);
     free(previous_hash);
     return -1;
}
