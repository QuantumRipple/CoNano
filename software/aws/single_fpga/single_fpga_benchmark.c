#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include <fpga_pci.h>
#include <fpga_mgmt.h>
#include <utils/lcd.h>

#include <utils/sh_dpi_tasks.h>

#include "../fpga_nano_lib/base/fpga_nano_pow.h"
#include "../fpga_nano_lib/wrapper/generate_work.h"
#include "../blake2/base/blake2.h"


/* use the stdout logger for printing debug information  */
const struct logger *logger = &logger_stdout;

int main(int argc, char **argv)
{
    printf("== RUNNING single_fpga_benchmark ==\n");

    printf("== GENERATING AND VALIDATING SEND POW ==\n");
    uint32_t *previous_hash = (uint32_t*)calloc(8, sizeof(uint32_t));

    pci_bar_handle_t pci_bar_handle = PCI_BAR_HANDLE_INIT;
    // single FPGA will be put into slot '0'
    int rc = setup_fpga_connection(&pci_bar_handle, 0 /* slot */);
    fail_on(rc, err, "Cannot setup FPGA connection\n");

    // clear any remaining pow in queue
    clear_pow_queue(pci_bar_handle);

    // benchmark send for 100 PoW
    int i;
    int items_in_queue = 0;
    int benchmark_number_send = 100;

    struct timeval start_time_send;
    gettimeofday(&start_time_send, NULL);
    for(i = 0; i < benchmark_number_send; i++)
    {
        // set to generate pow at send threshold
        rc = set_pow_to_send(&pci_bar_handle);
        fail_on(rc, err, "Cannot set_pow_to_send\n");

        rc = set_send_threshold(&pci_bar_handle, 35);
        fail_on(rc, err, "Cannot set_send_threshold\n");

        // set some bits in previous_hash and send for work generation
        previous_hash[0] = (uint32_t)i;
        input_hash(pci_bar_handle, previous_hash);
        items_in_queue++;
    }

    struct work_t work;
    while(items_in_queue > 0)
    {
        // pull until pow ready
        check_pow_output_ready(pci_bar_handle);

        // get work and pop from queue
        get_pow_output(pci_bar_handle, &work);
        pop_pow(pci_bar_handle);

        printf("== VALIDATING SEND POW %d of %d ==\n", benchmark_number_send - items_in_queue + 1, benchmark_number_send);
        validate_pow(&work);
        items_in_queue--;
    }

    struct timeval end_time_send;
    gettimeofday(&end_time_send, NULL); 

    printf("== GENERATING AND VALIDATING RECEIVE POW ==\n");

    struct timeval start_time_receive;
    gettimeofday(&start_time_receive, NULL); 
    int benchmark_number_receive = 500;
    
    // benchmark receive for 500 PoW (receive is about x64 easier then send)
    for(i = 0; i < benchmark_number_receive; i++)
    {
        // set to generate pow at receive threshold
        rc = set_pow_to_receive(&pci_bar_handle);
        fail_on(rc, err, "Cannot set_pow_to_receive\n");

        rc = set_receive_threshold(&pci_bar_handle, 41);
        fail_on(rc, err, "Cannot set_receive_threshold\n");

        // set some bits in previous_hash and send for work generation
        previous_hash[0] = (uint32_t)i;
        input_hash(pci_bar_handle, previous_hash);
        items_in_queue++;
    }

    while(items_in_queue > 0)
    {
        // pull until pow ready
        check_pow_output_ready(pci_bar_handle);

        // get work and pop from queue
        get_pow_output(pci_bar_handle, &work);
        pop_pow(pci_bar_handle);

        printf("== VALIDATING RECEIVE POW %d of %d ==\n", benchmark_number_receive - items_in_queue + 1, benchmark_number_receive);
        validate_pow(&work);
        items_in_queue--;
    }

    struct timeval end_time_receive;
    gettimeofday(&end_time_receive, NULL);  

    double send_total_time = (double)(end_time_send.tv_usec - start_time_send.tv_usec) / 1000000 + (double)(end_time_send.tv_sec - start_time_send.tv_sec);
    double receive_total_time = (double)(end_time_receive.tv_usec - start_time_receive.tv_usec) / 1000000 + (double)(end_time_receive.tv_sec - start_time_receive.tv_sec);

    printf("== SINGLE FPGA %.4f SEND POW PER SECOND ==\n", benchmark_number_send / send_total_time);
    printf("== TOTAL GENERATION TIME %.4f SECONDS FOR %d SEND POW ==\n", send_total_time, benchmark_number_send);

    printf("== SINGLE FPGA %.4f RECEIVE POW PER SECOND==\n", benchmark_number_receive / receive_total_time);
    printf("== TOTAL GENERATION TIME %.4f SECONDS FOR %d RECEIVE POW ==\n", receive_total_time, benchmark_number_receive);

    clean_up_connection(pci_bar_handle);

    free(work.previous_hash);
    free(work.work_value);
    free(work.nonce_value);
    free(previous_hash);
   return rc;
   err:
     clean_up_connection(pci_bar_handle);

     free(work.previous_hash);
     free(work.work_value);
     free(work.nonce_value);
     free(previous_hash);
     return -1;
}