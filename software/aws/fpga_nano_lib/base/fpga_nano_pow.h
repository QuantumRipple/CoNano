#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>
#include <inttypes.h>
#include <limits.h>

#include <fpga_pci.h>
#include <fpga_mgmt.h>
#include <utils/lcd.h>

#include <utils/sh_dpi_tasks.h>

/* Constants determined by the FPGA IP Core */
#define REVISION_INFO UINT64_C(0x00)

/* Commands, response control, and status to process previous_hash */
#define FIFO_STATUS_CONTROL UINT64_C(0x04)

/* Difficulty thresholds */
#define DIFFICULTY_THRESHOLDS UINT64_C(0x08)

/* Commands for stopping, starting, and clearing PoW queue*/
#define SYSTEM_CONTROL UINT64_C(0x0C)

/* 1-bit difficulty selection index (send 1, receive 0)*/
#define DIFFICULTY_SELECTION UINT64_C(0x1C)

/* 256-bit input previous_hash spread across 8 32-bit registers */
#define INPUT_PREVIOUS_HASH_ZERO UINT64_C(0x20)
#define INPUT_PREVIOUS_HASH_ONE UINT64_C(0x24)
#define INPUT_PREVIOUS_HASH_TWO UINT64_C(0x28)
#define INPUT_PREVIOUS_HASH_THREE UINT64_C(0x2C)
#define INPUT_PREVIOUS_HASH_FOUR UINT64_C(0x30)
#define INPUT_PREVIOUS_HASH_FIVE UINT64_C(0x34)
#define INPUT_PREVIOUS_HASH_SIX UINT64_C(0x38)
#define INPUT_PREVIOUS_HASH_SEVEN UINT64_C(0x3C)

/* 256-bit out previous_hash spread across 8 32-bit registers */
#define OUTPUT_PREVIOUS_HASH_ZERO UINT64_C(0x40)
#define OUTPUT_PREVIOUS_HASH_ONE UINT64_C(0x44)
#define OUTPUT_PREVIOUS_HASH_TWO UINT64_C(0x48)
#define OUTPUT_PREVIOUS_HASH_THREE UINT64_C(0x4C)
#define OUTPUT_PREVIOUS_HASH_FOUR UINT64_C(0x50)
#define OUTPUT_PREVIOUS_HASH_FIVE UINT64_C(0x54)
#define OUTPUT_PREVIOUS_HASH_SIX UINT64_C(0x58)
#define OUTPUT_PREVIOUS_HASH_SEVEN UINT64_C(0x5C)

/* 64-bit work nounce spread across 2 32-bit registers */
#define OUTPUT_WORK_NOUNCE_ONE UINT64_C(0x60)
#define OUTPUT_WORK_NOUNCE_TWO UINT64_C(0x64)

/* 64-bit work value (hash) spread across 2 32-bit registers */
#define OUTPUT_WORK_VALUE_ONE UINT64_C(0x68)
#define OUTPUT_WORK_VALUE_TWO UINT64_C(0x6C)

int get_revision_info(pci_bar_handle_t *pci_bar_handle, uint32_t* revision_info);

int setup_fpga_connection(pci_bar_handle_t *pci_bar_handle, int slot_id);
void clean_up_connection(pci_bar_handle_t pci_bar_handle);
int check_afi_ready(int slot_id);

int get_output_previous_hash(pci_bar_handle_t *pci_bar_handle, uint32_t* previous_hash);
int get_output_work_value(pci_bar_handle_t *pci_bar_handle, uint32_t* work_value);
int get_output_work_nounce(pci_bar_handle_t *pci_bar_handle, uint32_t* work_nounce);

int input_previous_hash(pci_bar_handle_t *pci_bar_handle, uint32_t* previous_hash);

int push_input(pci_bar_handle_t *pci_bar_handle);
int check_push_input_ready(pci_bar_handle_t *pci_bar_handle, uint32_t* status_control);

int pop_output(pci_bar_handle_t *pci_bar_handle);

int abort_work(pci_bar_handle_t *pci_bar_handle);
int stop_abort_work(pci_bar_handle_t *pci_bar_handle);

int dump(pci_bar_handle_t *pci_bar_handle);
int stop_dump(pci_bar_handle_t *pci_bar_handle);

int disable(pci_bar_handle_t *pci_bar_handle);
int enable(pci_bar_handle_t *pci_bar_handle);

int check_pow_ready(pci_bar_handle_t *pci_bar_handle, uint32_t* pow_ready);

int set_pow_to_send(pci_bar_handle_t *pci_bar_handle);
int set_pow_to_receive(pci_bar_handle_t *pci_bar_handle);
int get_pow_type(pci_bar_handle_t *pci_bar_handle, uint32_t* pow_type);

int get_receive_threshold(pci_bar_handle_t *pci_bar_handle, uint32_t* receive_threshold);
int get_send_threshold(pci_bar_handle_t *pci_bar_handle, uint32_t* send_threshold);

int set_receive_threshold(pci_bar_handle_t *pci_bar_handle, uint32_t value);
int set_send_threshold(pci_bar_handle_t *pci_bar_handle, uint32_t value);

/* Utils */
uint32_t get_bits(int startBit, int endBit, uint32_t value);
uint32_t set_bits(uint32_t orig_value, uint32_t to_set, int startBit, int endBit);

/* Debugging */
void print_binary(uint32_t number);
