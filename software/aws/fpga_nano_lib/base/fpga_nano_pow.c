#include "fpga_nano_pow.h"

/*
 * pci_vendor_id and pci_device_id values below are Amazon's and avaliable to use for a given FPGA slot. 
 */
static uint16_t pci_vendor_id = 0x1d0f; /* Amazon PCI Vendor ID */
static uint16_t pci_device_id = 0xf010; /* PCI Device ID preassigned by Amazon for F1 applications */

int get_output_previous_hash(pci_bar_handle_t *pci_bar_handle, uint32_t* previous_hash)
{
  int rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_PREVIOUS_HASH_ZERO, &(previous_hash[0]));
  fail_on(rc, err, "Cannot set OUTPUT_PREVIOUS_HASH_ZERO value");

  rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_PREVIOUS_HASH_ONE, &(previous_hash[1]));
  fail_on(rc, err, "Cannot set OUTPUT_PREVIOUS_HASH_ONE value");

  rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_PREVIOUS_HASH_TWO, &(previous_hash[2]));
  fail_on(rc, err, "Cannot set OUTPUT_PREVIOUS_HASH_TWO value");

  rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_PREVIOUS_HASH_THREE, &(previous_hash[3]));
  fail_on(rc, err, "Cannot set OUTPUT_PREVIOUS_HASH_THREE value");

  rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_PREVIOUS_HASH_FOUR, &(previous_hash[4]));
  fail_on(rc, err, "Cannot set OUTPUT_PREVIOUS_HASH_FOUR value");

  rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_PREVIOUS_HASH_FIVE, &(previous_hash[5]));
  fail_on(rc, err, "Cannot set OUTPUT_PREVIOUS_HASH_FIVE value");

  rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_PREVIOUS_HASH_SIX, &(previous_hash[6]));
  fail_on(rc, err, "Cannot set OUTPUT_PREVIOUS_HASH_SIX value");

  rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_PREVIOUS_HASH_SEVEN, &(previous_hash[7]));
  fail_on(rc, err, "Cannot set OUTPUT_PREVIOUS_HASH_SEVEN value");

  return rc;
  err:
    return -1;
}

int get_output_work_value(pci_bar_handle_t *pci_bar_handle, uint32_t* work_value)
{
  int rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_WORK_VALUE_ONE, &(work_value[0]));
  fail_on(rc, err, "Cannot set OUTPUT_WORK_VALUE_ONE value");

  rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_WORK_VALUE_TWO, &(work_value[1]));
  fail_on(rc, err, "Cannot set OUTPUT_WORK_VALUE_TWO value");

  return rc;
  err:
    return -1;
}


int get_output_work_nounce(pci_bar_handle_t *pci_bar_handle, uint32_t* work_nounce)
{
  int rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_WORK_NOUNCE_ONE, &(work_nounce[0]));
  fail_on(rc, err, "Cannot set OUTPUT_WORK_NOUNCE_ONE value");

  rc = fpga_pci_peek(*pci_bar_handle, OUTPUT_WORK_NOUNCE_TWO, &(work_nounce[1]));
  fail_on(rc, err, "Cannot set OUTPUT_WORK_NOUNCE_TWO value");
  
  return rc;
  err:
    return -1;
}

int pop_output(pci_bar_handle_t *pci_bar_handle)
{
  uint32_t status_control = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, FIFO_STATUS_CONTROL, &status_control);
  fail_on(rc, err, "Cannot get FIFO_STATUS_CONTROL value");
  
  status_control = set_bits(status_control, (uint32_t)1, 1, 1);
  rc = fpga_pci_poke(*pci_bar_handle, FIFO_STATUS_CONTROL, status_control);
  fail_on(rc, err, "Cannot set FIFO_STATUS_CONTROL to pop_output");

  return rc;
  err:
    return -1;
}

int push_input(pci_bar_handle_t *pci_bar_handle)
{
  uint32_t status_control = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, FIFO_STATUS_CONTROL, &status_control);
  fail_on(rc, err, "Cannot get FIFO_STATUS_CONTROL value");

  status_control = set_bits(status_control, (uint32_t)1, 4, 4);

  rc = fpga_pci_poke(*pci_bar_handle, FIFO_STATUS_CONTROL, status_control);
  fail_on(rc, err, "Cannot set FIFO_STATUS_CONTROL to push_input");

  return rc;
  err:
    return -1;
}

int check_push_input_ready(pci_bar_handle_t *pci_bar_handle, uint32_t* status_control)
{
  uint32_t value = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, FIFO_STATUS_CONTROL, &value);
  fail_on(rc, err, "Cannot set FIFO_STATUS_CONTROL value");
  
  *status_control = get_bits(4, 4, value);

  return rc;
  err:
    return -1;
}

int input_previous_hash(pci_bar_handle_t *pci_bar_handle, uint32_t* previous_hash)
{
  int rc = fpga_pci_poke(*pci_bar_handle, INPUT_PREVIOUS_HASH_ZERO, previous_hash[0]);
  fail_on(rc, err, "Cannot set INPUT_PREVIOUS_HASH_ZERO to input value");

  rc = fpga_pci_poke(*pci_bar_handle, INPUT_PREVIOUS_HASH_ONE, previous_hash[1]);
  fail_on(rc, err, "Cannot set INPUT_PREVIOUS_HASH_ONE to input value");

  rc = fpga_pci_poke(*pci_bar_handle, INPUT_PREVIOUS_HASH_TWO, previous_hash[2]);
  fail_on(rc, err, "Cannot set INPUT_PREVIOUS_HASH_TWO to input value");

  rc = fpga_pci_poke(*pci_bar_handle, INPUT_PREVIOUS_HASH_THREE, previous_hash[3]);
  fail_on(rc, err, "Cannot set INPUT_PREVIOUS_HASH_THREE to input value");

  rc = fpga_pci_poke(*pci_bar_handle, INPUT_PREVIOUS_HASH_FOUR, previous_hash[4]);
  fail_on(rc, err, "Cannot set INPUT_PREVIOUS_HASH_FOUR to input value");

  rc = fpga_pci_poke(*pci_bar_handle, INPUT_PREVIOUS_HASH_FIVE, previous_hash[5]);
  fail_on(rc, err, "Cannot set INPUT_PREVIOUS_HASH_FIVE to input value");

  rc = fpga_pci_poke(*pci_bar_handle, INPUT_PREVIOUS_HASH_SIX, previous_hash[6]);
  fail_on(rc, err, "Cannot set INPUT_PREVIOUS_HASH_SIX to input value");

  rc = fpga_pci_poke(*pci_bar_handle, INPUT_PREVIOUS_HASH_SEVEN, previous_hash[7]);
  fail_on(rc, err, "Cannot set INPUT_PREVIOUS_HASH_SEVEN to input value");

   return rc;
   err:
    return -1;
}

int check_pow_ready(pci_bar_handle_t *pci_bar_handle, uint32_t* pow_ready)
{
  uint32_t value = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, FIFO_STATUS_CONTROL, &value);
  fail_on(rc, err, "Cannot get FIFO_STATUS_CONTROL");

  *pow_ready = get_bits(0, 0, value);
  return rc;
  err:
    return -1;
}

int get_revision_info(pci_bar_handle_t *pci_bar_handle, uint32_t* revision_info)
{
  int rc = fpga_pci_peek(*pci_bar_handle, REVISION_INFO, revision_info);
  fail_on(rc, err, "Cannot get REVISION_INFO");

  return rc;
  err:
    return -1;
}

int get_pow_type(pci_bar_handle_t *pci_bar_handle, uint32_t* pow_type)
{
  int rc = fpga_pci_peek(*pci_bar_handle, DIFFICULTY_SELECTION, pow_type);
  fail_on(rc, err, "Cannot get DIFFICULTY_SELECTION");

  return rc;
  err:
    return -1;
}

int dump(pci_bar_handle_t *pci_bar_handle)
{
  uint32_t system_control_registor = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, SYSTEM_CONTROL, &system_control_registor);
  fail_on(rc, err, "Cannot get SYSTEM_CONTROL");

  system_control_registor = set_bits(system_control_registor, (uint32_t)1, 1, 1);

  rc = fpga_pci_poke(*pci_bar_handle, SYSTEM_CONTROL, system_control_registor);
  fail_on(rc, err, "Cannot set SYSTEM_CONTROL to dump");

  return 0;
  err:
    return -1;
}

int stop_dump(pci_bar_handle_t *pci_bar_handle)
{
  uint32_t system_control_registor = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, SYSTEM_CONTROL, &system_control_registor);
  fail_on(rc, err, "Cannot get SYSTEM_CONTROL");

  system_control_registor = set_bits(system_control_registor, (uint32_t)0, 1, 1);

  rc = fpga_pci_poke(*pci_bar_handle, SYSTEM_CONTROL, system_control_registor);
  fail_on(rc, err, "Cannot set SYSTEM_CONTROL to stop_dump");

  return 0;
  err:
    return -1;
}

int abort_work(pci_bar_handle_t *pci_bar_handle)
{
  uint32_t system_control_registor = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, SYSTEM_CONTROL, &system_control_registor);
  fail_on(rc, err, "Cannot get SYSTEM_CONTROL");

  system_control_registor = set_bits(system_control_registor, (uint32_t)1, 0, 0);

  rc = fpga_pci_poke(*pci_bar_handle, SYSTEM_CONTROL, system_control_registor);
  fail_on(rc, err, "Cannot set SYSTEM_CONTROL to abort");

  return 0;
  err:
    return -1;
}

int stop_abort_work(pci_bar_handle_t *pci_bar_handle)
{
  uint32_t system_control_registor = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, SYSTEM_CONTROL, &system_control_registor);
  fail_on(rc, err, "Cannot get SYSTEM_CONTROL");

  system_control_registor = set_bits(system_control_registor, (uint32_t)0, 0, 0);

  rc = fpga_pci_poke(*pci_bar_handle, SYSTEM_CONTROL, system_control_registor);
  fail_on(rc, err, "Cannot set SYSTEM_CONTROL to stop_abort");

  return 0;
  err:
    return -1;
}

int disable(pci_bar_handle_t *pci_bar_handle)
{
  uint32_t system_control_registor = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, SYSTEM_CONTROL, &system_control_registor);
  fail_on(rc, err, "Cannot get SYSTEM_CONTROL");

  system_control_registor = set_bits(system_control_registor, (uint32_t)1, 2, 2);

  rc = fpga_pci_poke(*pci_bar_handle, SYSTEM_CONTROL, system_control_registor);
  fail_on(rc, err, "Cannot set SYSTEM_CONTROL to disable");

  return 0;
  err:
    return -1;
}

int enable(pci_bar_handle_t *pci_bar_handle)
{
  uint32_t system_control_registor = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, SYSTEM_CONTROL, &system_control_registor);
  fail_on(rc, err, "Cannot get SYSTEM_CONTROL");

  system_control_registor = set_bits(system_control_registor, (uint32_t)0, 2, 2);

  rc = fpga_pci_poke(*pci_bar_handle, SYSTEM_CONTROL, system_control_registor);
  fail_on(rc, err, "Cannot set SYSTEM_CONTROL to enable");

  return 0;
  err:
    return -1;
}

int set_pow_to_receive(pci_bar_handle_t *pci_bar_handle)
{
  int rc = fpga_pci_poke(*pci_bar_handle, DIFFICULTY_SELECTION, (uint32_t)0);
  fail_on(rc, err, "Cannot set PoW type to receive");

  return 0;
  err:
    return -1;
}

int set_pow_to_send(pci_bar_handle_t *pci_bar_handle)
{
  int rc = fpga_pci_poke(*pci_bar_handle, DIFFICULTY_SELECTION, (uint32_t)1);
  fail_on(rc, err, "Cannot set PoW type to send");

  return 0;
  err:
    return -1;
}

int set_receive_threshold(pci_bar_handle_t *pci_bar_handle, uint32_t threshold)
{
  uint32_t diff_register = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, DIFFICULTY_THRESHOLDS, &diff_register);
  fail_on(rc, err, "Cannot get DIFFICULTY_THRESHOLDS registor");

  diff_register = set_bits(diff_register, threshold, 0, 5);

  rc = fpga_pci_poke(*pci_bar_handle, DIFFICULTY_THRESHOLDS, diff_register);
  fail_on(rc, err, "Cannot set DIFFICULTY_THRESHOLDS receive");

  return 0;
  err:
    return -1;
}

int set_send_threshold(pci_bar_handle_t *pci_bar_handle, uint32_t threshold)
{
  uint32_t diff_register = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, DIFFICULTY_THRESHOLDS, &diff_register);
  fail_on(rc, err, "Cannot get DIFFICULTY_THRESHOLDS registor");

  diff_register = set_bits(diff_register, threshold, 8, 13);

  rc = fpga_pci_poke(*pci_bar_handle, DIFFICULTY_THRESHOLDS, diff_register);
  fail_on(rc, err, "Cannot set DIFFICULTY_THRESHOLDS send");

  return 0;
  err:
    return -1;
}

int get_receive_threshold(pci_bar_handle_t *pci_bar_handle, uint32_t* receive_threshold)
{
  uint32_t value = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, DIFFICULTY_THRESHOLDS, &value);
  fail_on(rc, err, "Cannot get DIFFICULTY_THRESHOLDS receive");

  *receive_threshold = get_bits(0, 5, value);
  return rc;
  err:
    return -1;
}

int get_send_threshold(pci_bar_handle_t *pci_bar_handle, uint32_t* send_threshold)
{
  uint32_t value = 0;
  int rc = fpga_pci_peek(*pci_bar_handle, DIFFICULTY_THRESHOLDS, &value);
  fail_on(rc, err, "Cannot get DIFFICULTY_THRESHOLDS send");

  *send_threshold = get_bits(8, 13, value);
  return rc;
  err:
    return -1;
}

  
int setup_fpga_connection(pci_bar_handle_t *pci_bar_handle, int slot_id)
{
    int rc = fpga_mgmt_init();
    fail_on(rc, err, "Unable to initialize the fpga_mgmt library");
    
    rc = fpga_pci_init();
    fail_on(rc, err, "Unable to initialize the fpga_pci library");

    rc = check_afi_ready(slot_id);
    fail_on(rc, err, "AFI not ready");

    rc = fpga_pci_attach(slot_id, FPGA_APP_PF, APP_PF_BAR1, 0, pci_bar_handle);
    fail_on(rc, err, "Unable to attach to the AFI on slot id %d", slot_id);

    return rc;
    err:
      return -1;
}

int check_afi_ready(int slot_id) {
   struct fpga_mgmt_image_info info = {0};

   int rc = fpga_mgmt_describe_local_image(slot_id, &info, 0);
   fail_on(rc, err, "Unable to get AFI information from slot %d. Are you running as root?",slot_id);

   if (info.status != FPGA_STATUS_LOADED)
   {
     rc = -1;
     fail_on(rc, err, "AFI in Slot %d is not in READY state !", slot_id);
   }

   if (info.spec.map[FPGA_APP_PF].vendor_id != pci_vendor_id || info.spec.map[FPGA_APP_PF].device_id != pci_device_id)
   {
     rc = fpga_pci_rescan_slot_app_pfs(slot_id);
     fail_on(rc, err, "Unable to update PF for slot %d", slot_id);

     rc = fpga_mgmt_describe_local_image(slot_id, &info, 0);
     fail_on(rc, err, "Unable to get AFI information from slot %d", slot_id);

     if (info.spec.map[FPGA_APP_PF].vendor_id != pci_vendor_id || info.spec.map[FPGA_APP_PF].device_id != pci_device_id)
     {
       rc = -1;
       fail_on(rc, err, "The PCI vendor id and device of the loaded AFI are not the expected values.");
     }
   }

   return rc;
   err:
      return -1;
}


/*
* Close out FPGA connection on error or completion
*/
void clean_up_connection(pci_bar_handle_t pci_bar_handle)
{
      int rc;
      if (pci_bar_handle >= 0)
      {
        rc = fpga_pci_detach(pci_bar_handle);
        if (rc)
        {
            printf("Failure while detaching from the FPGA.\n");
        }
      }

}

/*
* Gets input bits from [startBit, endBit]
*/
uint32_t get_bits(int startBit, int endBit, uint32_t input)
{
    uint32_t mask = (1 << (endBit - startBit + 1)) - 1;
    return (input >> startBit) & mask;
}

/*
Set orig_value bits between startBit and endBit with the first startBit - endBit of to_set
*/
uint32_t set_bits(uint32_t orig_value, uint32_t to_set, int startBit, int endBit) 
{
    int allOnes = ~0; 

    int left = allOnes << (startBit);
    int right = ((1 << endBit)) - 1;

    int mask = ~(left & right); 

    int masked_n = orig_value & mask; 

    int m_shifted = to_set << startBit;

    return (masked_n | m_shifted);
} 

/*
Prints out the value of 'number' in binary to help debug
*/
void print_binary(uint32_t number)
{
  int i;
  for (i = 0; i < 32; ++i) {
    if (number >> i & 0x1) {
      putchar('1');
    }
    else {
      putchar('0');
    }
  }
  putchar('\n');
}