#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <fpga_pci.h>
#include <fpga_mgmt.h>
#include <utils/lcd.h>

#include <utils/sh_dpi_tasks.h>

#include "../../fpga_nano_lib/base/fpga_nano_pow.h"
#include "../../blake2/base/blake2.h"

struct work_t
{
	uint32_t *previous_hash;
	uint32_t *work_value;
	uint32_t *nonce_value;

} work_t;

int input_hash(pci_bar_handle_t pci_bar_handle, uint32_t *previous_hash);

int check_pow_output_ready(pci_bar_handle_t pci_bar_handle);

int get_pow_output(pci_bar_handle_t pci_bar_handle, struct work_t *work);

int pop_pow(pci_bar_handle_t pci_bar_handle);

int validate_pow(struct work_t *work);

int clear_pow_queue(pci_bar_handle_t pci_bar_handle);