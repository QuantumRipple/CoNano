#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <fpga_pci.h>
#include <fpga_mgmt.h>
#include <utils/lcd.h>

#include <utils/sh_dpi_tasks.h>

#include "../../fpga_nano_lib/base/fpga_nano_pow.h"
#include "../../fpga_nano_lib/wrapper/generate_work.h"
#include "../../blake2/base/blake2.h"

int input_hash(pci_bar_handle_t pci_bar_handle, uint32_t *previous_hash)
{
	// status will be 0 when ready to accept/has accepted value
    uint32_t queue_ready_to_accept_input = 1;
    int rc;
    while(queue_ready_to_accept_input == 1)
    {
        /*
        printf("Checking queue ready to accept previous_hash\n");
        */
        rc = check_push_input_ready(&pci_bar_handle, &queue_ready_to_accept_input);
        fail_on(rc, err, "Cannot get queue_ready_to_accept_input value");
        
        /*
        printf("%s\n", "queue_ready_to_accept_input:");
        print_binary(queue_ready_to_accept_input);
        sleep(1);
        */
    }
    rc = input_previous_hash(&pci_bar_handle, previous_hash);
    fail_on(rc, err, "Cannot set previous_hash input value");
    
    rc = push_input(&pci_bar_handle);
    fail_on(rc, err, "Cannot push_input");
    queue_ready_to_accept_input = (uint32_t)1;

    while(queue_ready_to_accept_input == (uint32_t)1)
    {
        /*
        printf("previous_hash push processing\n");
        */

        rc = check_push_input_ready(&pci_bar_handle, &queue_ready_to_accept_input);
        fail_on(rc, err, "Cannot get queue_ready_to_accept_input value");
        
        /*
        printf("%s\n", "queue_ready_to_accept_input:");
        print_binary(queue_ready_to_accept_input);
        sleep(1);
        */
    }

    return rc;
    err:
    	return -1;
}

int check_pow_output_ready(pci_bar_handle_t pci_bar_handle)
{
    uint32_t pow_ready = (uint32_t)0;
    int rc;
    while(pow_ready == (uint32_t)0)
    {   /*
        printf("generating PoW on FPGA\n");
        */
        rc = check_pow_ready(&pci_bar_handle, &pow_ready);
        fail_on(rc, err, "Cannot get check_pow_ready value");
        /*
        printf("%s\n", "pow_ready:");
        print_binary(pow_ready);
        sleep(1);
        */
    }

    return rc;
    err:
    	return -1;
}

int get_pow_output(pci_bar_handle_t pci_bar_handle, struct work_t *work)
{
    uint32_t *previous_hash = calloc(8, sizeof(uint32_t));
    uint32_t *work_value = calloc(2, sizeof(uint32_t));
    uint32_t *nonce_value = calloc(2, sizeof(uint32_t));

    int rc = get_output_previous_hash(&pci_bar_handle, previous_hash);
    fail_on(rc, err, "Cannot get_output_previous_hash value");

    rc = get_output_work_value(&pci_bar_handle, work_value);
    fail_on(rc, err, "Cannot get_output_work_value value");

    rc = get_output_work_nounce(&pci_bar_handle, nonce_value);
    fail_on(rc, err, "Cannot get_output_work_nounce value");

    work->previous_hash = previous_hash;
    work->work_value = work_value;
    work->nonce_value = nonce_value;

    return rc;
    err:
    	return -1;
}

int pop_pow(pci_bar_handle_t pci_bar_handle)
{
    int rc = pop_output(&pci_bar_handle);
    fail_on(rc, err, "Cannot pop_output");

    return rc;
    err:
    	return -1;
}

int validate_pow(struct work_t *work)
{
	uint8_t validate_fpga_hash_input[40];
    memset(&validate_fpga_hash_input, 0, 40*sizeof(uint8_t));
    
    uint8_t previous_hash_validate[32];
    memcpy(&previous_hash_validate, work->previous_hash, 32*sizeof(uint8_t));

    memcpy(&validate_fpga_hash_input, work->nonce_value, 8*sizeof(uint8_t));
    memcpy(&(validate_fpga_hash_input[8]), work->previous_hash, 32*sizeof(uint8_t));

    uint8_t hash_output_c[8];
    memset(&hash_output_c, 0, 8*sizeof(uint8_t));

    blake2b(&hash_output_c, 8, &validate_fpga_hash_input, 40, NULL, 0);

    assert(memcmp(&hash_output_c, work->work_value, 8*sizeof(uint8_t)) == 0);

    return 0;
}

int clear_pow_queue(pci_bar_handle_t pci_bar_handle)
{
    uint32_t pow_ready = (uint32_t)1;

    int rc;
    while(pow_ready == (uint32_t)1)
    {   /*
        printf("generating PoW on FPGA\n");
        */
        rc = pop_pow(pci_bar_handle);
        fail_on(rc, err, "Cannot pop_pow from queue");

        rc = check_pow_ready(&pci_bar_handle, &pow_ready);
        fail_on(rc, err, "Cannot get check_pow_ready value");
        /*
        printf("%s\n", "pow_ready:");
        print_binary(pow_ready);
        */
        usleep(100);
    }

    return rc;
    err:
        return -1;
}