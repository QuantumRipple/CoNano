CoNano
======

CoNano is an FPGA CoProcessor/Accelerator core to generate work (hashes) for the Nano Cryptocurrency.  
Created under the MIT License, the project is intended to be used and adapted by anybody for any purpose with attribution.

The project is written in SystemVerilog (FPGA logic), Python (PynqZ2 driver), and C (AWS driver).  
FPGA logic is centered around a BLAKE2b hashing engine, and the project includes a driver to communicate with it.  
There is no directly usable application software in this project. The expected end user is an application developer with a use case for accelerated work generation.

Driver software fills a task queue in the FPGA with "previous block" hashes. FPGA logic will process this queue one item at a time, generating many BLAKE2b hashes for each task with an auto-incrementing nonce. When a (nonce,output_hash) tuple that meets a configurable difficulty threshold is found, it is inserted into a result queue.  
The queues provide a simple interface while permitting the FPGA to have 100% hashing uptime despite slow processors, low IO bandwidth, and/or high communication latency. The quantity of internal hashing that occurs vastly outweighs data in or out of the core, making IO performance non-critical.  
Two difficulty thresholds can be configured for seamless swapping per-task. This allows finding nonces for both send/change blocks and the much easier recieve blocks.

FPGA logic was built and simulated using Xilinx's Vivado 2020.2, although the raw HDL sources are largely vendor agnostic.  

The project was primarily designed and tested targeting a Xilinx Zynq-7020 on a PynqZ2 dev board (~$100 for ~50k LUT6), but has also been scaled up and tested for an Amazon AWS EC2 F1 instance (~1000k LUT6). The PynqZ2 implementation has been heavliy optimized and is quite close to the part's performance limits. Implementations on the larger AWS instances still have a great deal of room for improvement, as both developer time and build time to get maximal performance scales with part size. There is [a separate doc](doc/scaling_to_different_target_FPGA.txt) with details on porting to other FPGA targets of varying sizes.  

![block diagram](doc/CoNano_block_diagram.svg)  
Block Diagram of the FPGA core

PynqZ2 target
-------------
A small, cheap, and accessible FPGA dev board, the PynqZ2 is still much larger than most FPGA boards at this price point, and large enough for a single processing tile of the CoNano engine. The Zynq parts also include a full featured ARM processor system (PS) with a memory mapped AXI communication path directly to the FPGA (PL) portion. The driver software for this target runs under Linux on this ARM processor.  
The primary build for this target is 1 tile at 163 MHz and produces 0.31 Send/Change work per second (on par with an AMD R9-3900X).  
As a result of the tight optimization of the PynqZ2 target implementation, it draws significantly more power than can be output by the board's power supply under the factory configuration. More details can be found in [another doc](doc/pynqz2_power_limitations.txt). To run this on an unmodified PynqZ2 board, the conano_pynqz2_87MHz_1V3A.xsa bitstream should be used (or built from source for 87MHz). That does, however, leave performance on the table. The higher speed/performance 163MHz version has been tested successfully in hardware with modified power supply settings.  
The [Vivado project file](vivado_project/conano_pynqz2.xpr) for this target can be opened directly with free (WebPack) version of Vivado 2020.2 on your local machine and built to generate an XSA (bitstream plus information related to PS clock configuration bundled together). Builds will usually take an hour or more, and three pre-built images that all met timing are provided in the **artifacts** folder.

AWS EC2 F1 target
-----------------
Amazon cloud has seriously large and expensive FPGAs available for rent at a relatively low price. Unfortunately, they are somewhat handicapped by Amazon's large and required "helper" block that wastes resources on things that are of no benefit to this design like DDR and DMA controllers as well as an atypical build flow.
This target design was last built with 12 tiles at 150 MHz, but there is significant room for improvement (albiet high effort) on both of those parameters. Floorplanning is required to get a good build.  
Even without further optimization, this build produced 3.36 Send/Change work per second (on par with an Nvidia GTX1080).  
In order to target an AWS EC2 F1 instance, the Amazon cloud developer tools must be used, specifically the Vivado Developer AMI. Vivado's free license is not suitable for these parts, and the AMI has futher modifications to Vivado to support Amazon's special build flow.  
The F1 instances are physically connected to another AWS compute instance via PCIe, which was the communication path used for this driver.  
The AWS EC2 F1 variant is not fully up to date like the PynqZ2 variant, but there have been no significant functional changes since it was last in operation on AWS cloud hardware. When configured for larger FPGAs with many parallel hashing engines like this target, the overall CoNano core still processes tasks one at a time, it just takes proportionally less time to find a suitable nonce.

Authors
-------
FPGA logic written by **QuantumRipple** (nano_3qibg6wm9mzbmgjaoi4ra63bgcdgupi6opjqbbxyugc1n1y3gxphmtrxf3z4)  
Driver source written by **silverstar194** (donation address TBD)

Links/References
----------------
* BLAKE2b computation was designed with reference to the [specification](https://www.blake2.net/blake2.pdf).
* The [BLAKE2b reference C code](https://github.com/BLAKE2/BLAKE2/blob/master/ref/blake2b-ref.c) was used to generate various test vectors and intermediate test values during development.
* A valid set of Nano previous_hash/nonce/hash values (to use as a test vector) were scraped from the [nano-work-server](https://github.com/nanocurrency/nano-work-server) project's README.
* CoNano was utilized for practical application as a low-power work server by James Coxon. Details at https://github.com/jamescoxon/fpga_work_server.